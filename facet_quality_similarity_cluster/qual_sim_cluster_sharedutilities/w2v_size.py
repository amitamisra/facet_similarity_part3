import gensim

from qual_sim_cluster_sharedutilities import file_utilities


def get_word_count(w2v_model):
    wc = 0
    for vocab_item in w2v_model.vocab.values():
        wc += vocab_item.count

    return len(w2v_model.vocab.keys()), wc


# w2v = gensim.models.Word2Vec.load_word2vec_format(file_utilities.load_resource("w2v/google_300.bin"), binary=True)
# print("Google model has a vocab size of {0} with {1} total words".format(*get_word_count(w2v)))
#
# del w2v

w2v = gensim.models.Word2Vec.load_word2vec_format(file_utilities.load_resource("w2v/w2v_300.model"))
print("Custom model has a vocab size of {0} with {1} total words".format(*get_word_count(w2v)))
