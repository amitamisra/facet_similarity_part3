'''
Created on May 1, 2016
extract text, author information from database
@author: amita
'''

text84="""True. That's why Jesus gave up on the prostitutes and tax collectors. Er, waitaminnit....
I'll leave it to "these people" to decide what they are or aren't interested in finding. But speaking as a Christian, I think their questions are entirely legitimate, and one can learn a lot by taking their questions seriously instead of dismissing them as uninterested in the truth.
How do you know that?
Indeed, how do you know that some haven't truly found him?
Have you considered the possibility that maybe you are the one who isn't looking very hard? or that maybe, whatever you may think you've found, you are mistaken?
I confess that I'm not sure "they" are the ones who fear that hell may awaith them...."""




from db.db_connect import setup_connection
def _get_post_text(sql_session, dataset_id, discussion_id, post_id):
    """
    Returns the posttext for a given post.
    :param sql_session: sqlalchemy session
    :param dataset_id:
    :param discussion_id:
    :param post_id:
    :return: text for post
    """
    results = list(sql_session.execute("""
                select  text
                from  
                posts as p 
                join texts as t on 
                t.dataset_id= p.dataset_id and t.text_id=p.text_id
        where
            t.dataset_id = :dataset_id and
            discussion_id = :discussion_id and
            post_id = :post_id            
    """, {"dataset_id": dataset_id, "discussion_id": discussion_id, "post_id": post_id}))

    if len(results) == 1:
        return results[0][0]
    elif len(results) > 1:
        raise "Query returned too many results"
    else:
        return None

def _get_author_post(sql_session, dataset_id, discussion_id, post_id):
    """
    Returns the authorid for a given post.
    :param sql_session: sqlalchemy session
    :param dataset_id:
    :param discussion_id:
    :param post_id:
    :return: author_id
    """
    results = list(sql_session.execute("""
                select  author_id
                from  
                posts 
        where
            dataset_id = :dataset_id and
            discussion_id = :discussion_id and
            post_id = :post_id
    """, {"dataset_id": dataset_id, "discussion_id": discussion_id, "post_id": post_id}))

    if len(results) == 1:
        return results[0][0]
    elif len(results) > 1:
        raise "Query returned too many results"
    else:
        return None
if __name__ == '__main__':
    _, sql_session = setup_connection()
    forums_author=_get_author_post(sql_session, 1, 17, 7)
    convinceme_author=_get_author_post(sql_session, 2, 2750, 1)
    createdebate_author=_get_author_post(sql_session, 3, 31095, 1)
    assert forums_author==5
    assert convinceme_author==450
    assert createdebate_author==219 
    text=_get_post_text(sql_session, 1, 9122,84)
    assert text==text84
