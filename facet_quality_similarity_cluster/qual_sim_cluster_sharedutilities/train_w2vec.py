import sqlalchemy
from sqlalchemy import text
from _sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata
import gensim
import re
import nltk
import time

print ("working?")

"""
*****************************************************
This script is used to train the word2vec model over IAC.
*****************************************************
"""

def tokenize(text):
	# Function to convert a post to tokens.
	# 1) Splits sentences based on NLTK's sentence tonizor
	# 2) Removes non alphabetical characters
	# 3) Word tokenization
	sentences = nltk.sent_tokenize(text)
	sent_tokens = []
	for sentence in sentences:
		sentence = re.sub("[^a-zA-Z]"," ", sentence)
		word_tokens = nltk.word_tokenize(sentence)
		sent_tokens.append(word_tokens)
	return sent_tokens


# SQL command to load data page by page
cmd= "SELECT  t.text \
FROM    ( \
		SELECT  dataset_id, discussion_id \
		FROM    discussions \
		ORDER BY \
				dataset_id, discussion_id LIMIT :offset, :page_length \
		) q \
JOIN    posts p \
ON      p.dataset_id = q.dataset_id and \
	p.discussion_id = q.discussion_id \
JOIN 	texts t \
ON	t.dataset_id = p.dataset_id and \
	t.text_id = p.text_id \
;"


class IacText(object):
	def __init__(self, page_length = 1000, end = 1000):
		self.page_length = page_length
		self.end = end
 
	def __iter__(self):
		offset = 0
		while offset <= self.end:
			# query the data
			query = text(cmd)
			try:
			   result = sql_session.execute(query, {'offset' : offset, 'page_length' : self.page_length}).fetchall()
			except:
				offset += self.page_length
				print ("Query error: "+ str(offset))
				continue
			# tokenize results
			for r in result:
				try:
					sents = tokenize(r[0])
				except:
					print ("tokenize error: "+ str(offset))
					continue
				else:
					for s in sents:
						yield(s)
			# yield
			# increment page
			offset += self.page_length
			print (offset)


iactext = IacText(10000, 3822241) # a memory-friendly iterator
start_time = time.time()
model = gensim.models.Word2Vec(iactext, workers = 4, min_count = 10, size = 300)
elapsed_time = time.time() - start_time
print (elapsed_time)
model.save('./models/w2vec.300')
print (model.similarity('woman', 'man'))