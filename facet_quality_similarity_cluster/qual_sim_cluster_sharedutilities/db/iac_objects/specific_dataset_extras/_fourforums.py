from collections import defaultdict, Counter

from qual_sim_cluster_sharedutilities.db.iac_objects import Dataset


class FourforumsDataset(Dataset):
    def load_extras(self, sql_session):
        try:
            #stance
            author_stance_votes, labels, sides = self._get_author_stance_labels(sql_session)
            self.stance_votes = author_stance_votes  # {topic -> {author_id -> {side1:3, side2:1, other:1} } }
            self.stance_labels = labels  # {topic -> {author_id -> side1} }    'other' possible, 'mixed' if there are annots, but no obvious side
            self.stance_sides = sides  # {topic -> [side1, side2]}

            #2010 annots
            qr_task1_all, qr_task1_indexed = self._load_qr_task1(sql_session)
            self.qr_task1_averages_all = qr_task1_all
            self.qr_task1_averages_indexed = qr_task1_indexed

        except:
            pass

    def _load_stances(self):
        return {}

    def _load_post_stances(self):
        return {}


    def _get_author_stance_labels(self, sql_session):
        query_str = """select
      mturk_author_stance.topic_id as topic_id,
      dataset_id,
      discussion_id,
      topic,
      authors.author_id as author_id,
      username,
      topic_stance_id_1,
      stances1.stance as topic_stance_1,
      topic_stance_votes_1,
      topic_stance_id_2,
      stances2.stance as topic_stance_2,
      topic_stance_votes_2,
      topic_stance_votes_other
     from mturk_author_stance
      join authors using(dataset_id, author_id)
      join discussions using(dataset_id, discussion_id)
      join topics on (discussions.topic_id = topics.topic_id)
      join topic_stances as stances1 on (stances1.topic_id = mturk_author_stance.topic_id and stances1.topic_stance_id = mturk_author_stance.topic_stance_id_1)
      join topic_stances as stances2 on (stances2.topic_id = mturk_author_stance.topic_id and stances2.topic_stance_id = mturk_author_stance.topic_stance_id_2)
     where discussions.topic_id = mturk_author_stance.topic_id
     """
        query_result = sql_session.execute(query_str)
        raw_annots = {(entry['topic'], entry['dataset_id'], entry['discussion_id'], entry['author_id']): entry for entry in map(dict, query_result)}

        author_stance_votes = defaultdict(lambda: defaultdict(Counter))
        sides = defaultdict(list)
        for (topic, dataset_id, discussion_id, author_id), entry in raw_annots.items():
            author_stance_votes[topic][author_id][entry['topic_stance_1']] += entry['topic_stance_votes_1']
            author_stance_votes[topic][author_id][entry['topic_stance_2']] += entry['topic_stance_votes_2']
            author_stance_votes[topic][author_id]['other'] += entry['topic_stance_votes_other']
            if sides.get(topic) is None:
                sides[topic].append(entry['topic_stance_1'])
                sides[topic].append(entry['topic_stance_2'])
            else:
                assert entry['topic_stance_1'] in sides[topic] and \
                       entry['topic_stance_2'] in sides[topic]

        labels = defaultdict(dict)
        for topic in author_stance_votes.keys():
            for author_id, stance_votes in author_stance_votes[topic].items():
                total_votes = sum(stance_votes.values())

                if stance_votes[sides[topic][0]] >= stance_votes[sides[topic][1]]:
                    bigger_side = sides[topic][0]
                    smaller_side = sides[topic][1]
                else:
                    bigger_side = sides[topic][1]
                    smaller_side = sides[topic][0]

                # Majority label or solid plurality, default to 'mixed' to distinguish it from no annots
                if stance_votes[bigger_side] > total_votes / 2:
                    labels[topic][author_id] = bigger_side
                elif stance_votes['other'] > total_votes / 2:
                    labels[topic][author_id] = 'other'
                elif stance_votes[bigger_side] > stance_votes['other'] and stance_votes[bigger_side] >= 2 * stance_votes[smaller_side]:
                    labels[topic][author_id] = bigger_side
                else:
                    labels[topic][author_id] = 'mixed'

            # TODO: author_id instead of name
            # if topic == 'death penalty':
            #     labels[topic].update({'NATO 556': 'supports death penalty', 'wilted_laughter': 'opposes death penalty', 'Zardoz': '?', 'Phunting': '?', 'gman': '?', 'ThunderMelody': '?', 'Patriot': '?', 'VTCruiser': '?', 'Hunter': 'opposes death penalty', 'josephdphillips': 'supports death penalty', 'Truthsayer': '?', 'DarKnight': 'supports death penalty', 'Symbiote': 'opposes death penalty', 'Michele': 'opposes death penalty', 'Clive14': 'opposes death penalty', 'Polarkat52': 'opposes death penalty', 'admin': '?', 'J Miro': 'opposes death penalty', 'HeavenBound': 'supports death penalty', 'olivortex': 'opposes death penalty', 'enfant terrible': '?', 'grizzly6626': 'supports death penalty', 'jyoshu': '?', 'iangb': '?', 'andjustice4all': '?', 'Nefertari': '?', 'Phyllis': '?', 'The_Comedian': '?', 'REMF Hunter': 'supports death penalty', 'Dr Ovid': 'supports death penalty', 'WhiteFalcon': '?', 'antonia': '?', 'SilentPoet': '?', 'Axolotl': '?', 'chester': 'opposes death penalty', 'Ballentine': '?'})

        return author_stance_votes, labels, sides


    def _load_qr_task1(self, sql_session):
        query_str = """
    select *
     from mturk_2010_qr_task1_average_responses
      join mturk_2010_qr_entries using(page_id, tab_number)
     where discussion_id is not null;"""
        #TODO: consider the 28 entries that don't have a discussion_id (aren't in the present dataset)

        query_result = sql_session.execute(query_str)
        raw_annots = {(entry['page_id'], entry['tab_number']): entry for entry in map(dict, query_result)}
        indexed_annots = defaultdict(lambda: defaultdict(dict))  # {discussion_id -> {post_id -> {key -> entry}}

        duplicates = list()
        for key, entry in raw_annots.items():
            entry['key'] = key

            if indexed_annots[entry['discussion_id']][entry['post_id']].get(entry['quote_index']) is None:
                indexed_annots[entry['discussion_id']][entry['post_id']][entry['quote_index']] = entry
            else:
                duplicates.append(entry)
                entry['duplicates'] = indexed_annots[entry['discussion_id']][entry['post_id']][entry['quote_index']]['key']

        #TODO, consider the 6 duplicates, note that they are in raw_annots

        return raw_annots, indexed_annots
