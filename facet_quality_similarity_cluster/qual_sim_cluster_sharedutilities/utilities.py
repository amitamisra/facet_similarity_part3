'''
Created on Jan 31, 2016
utility function that may be needed across clustering algorithms
@author: amita
'''

from pandas import DataFrame
#create a symmetric dataframe, 
#inputs
#dataframe: dataframe with pairs as input
#simcol: field in dataframe to be used for creating a symmetric matrix
#id1:unique record id for one pair
#id2:unique record id for another pair
def dataframe_to_symm_df(dataframe,simcol,id1,id2):
    
    id_list1 = dataframe[id1].tolist()
    id_list2 = dataframe[id2].tolist()
    header=set( id_list1+id_list2)
    dfsymm= DataFrame( index=header, columns=header)
    records=len(dataframe)
    for i in range(0,records):
        row=dataframe.iloc[i][id1]
        col=dataframe.iloc[i][id2]
        sim_value=dataframe.iloc[i][simcol]
        dfsymm.set_value(row,col,sim_value)
        dfsymm.set_value(col,row,sim_value)
        dfsymm.set_value(row,row,5)
        dfsymm.set_value(col,col,5)
    return dfsymm


def valuation_formula(x,dictcluster):
    return dictcluster[x]

#dictcluster: dictionary with keys as unique ids
def addclusterlabels(dbscanValuesObj,dictcluster,id1,id2):
    dbscanValuesObj.dataframe["cluster_id1"]=dbscanValuesObj.dataframe.apply(lambda row: valuation_formula(row[id1], dictcluster), axis=1)
    dbscanValuesObj.dataframe["cluster_id2"]=dbscanValuesObj.dataframe.apply(lambda row: valuation_formula(row[id2], dictcluster), axis=1)
    
 
if __name__ == '__main__':
    pass