'''
Created on May 25, 2016
create a parser object to use for stanford parser
@author: amita
'''

import os,sys
from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities.stanford_corenlp_pywrapper.sockwrap import CoreNLP
import pandas as pd


def getparse(): 
        corenlp_path = file_utilities.load_resource("stanford-corenlp-full-2015-12-09")
        pos_tagger_path = file_utilities.load_resource("stanford-postagger-2015-12-09")
        srparser_path = file_utilities.load_resource("stanford-srparser-2014-10-23-models.jar")
        if not os.path.exists(corenlp_path):
            print("\n\tMissing corenlp. Please download from http://stanfordnlp.github.io/CoreNLP/\n"
                      "\tAfter downloading, uznip the file in resources, exiting the program")
            sys.exit(-1)
        if not os.path.exists(pos_tagger_path):
            print("\n\tMissing pos tagger models. Please download from "
                      "http://nlp.stanford.edu/software/tagger.html#ReleaseHistory\n"
                      "\tAfter downloading, uznip the file in resources")
            print(-1)
        if not os.path.exists(srparser_path):
            print("\n\tMissing shift-reduce parser models. Please download models from "
                      "http://nlp.stanford.edu/software/stanford-srparser-2014-10-23-models.jar.\n"
                      "\tAfter Downloading, place the models jar file in resources")
            sys.exit(-1)
        srparse_config = file_utilities.load_resource("config/srparse.properties")
        
        try:
            parser = CoreNLP(configfile=srparse_config,
                             corenlp_jars=[os.path.join(corenlp_path, "*"),
                                           os.path.join(pos_tagger_path, "*"),
                                           srparser_path])
        except Exception as e:
            print("Failed to load parser: {0}".format(str(e)))  
        return parser      
def func(row):
    return row["sentence_1"]
if __name__ == '__main__':
    parser=getparse()
    input_csv="/Users/amita/git/argument_summarization/data/input.deft-forum.csv"
    #df=pd.read_csv(input_csv)
    #text=df.apply(func,axis=1)
    #print("text")
    text="Hello how are you doing? I am enjoying"
    parsed_doc = parser.parse_doc(text)
    print (parsed_doc)
    
    
    
    {'sentences': [{'parse': '(ROOT (SBARQ (UH Hello) (WHADVP (WRB how)) (SQ (VBP are) (NP (PRP you)) (VP (VBG doing))) (. ?)))', 
                    'deps_basic': [['root', -1, 4], ['dep', 4, 0], ['advmod', 4, 1], ['aux', 4, 2], ['nsubj', 4, 3], ['punct', 4, 5]],
                     'char_offsets': [[0, 5], [6, 9], [10, 13], [14, 17], [18, 23], [23, 24]], 
                     'normner': ['', '', '', '', '', ''], 
                     'lemmas': ['hello', 'how', 'be', 'you', 'do', '?'], 
                     'pos': ['UH', 'WRB', 'VBP', 'PRP', 'VBG', '.'], 
                     'deps_cc': [['root', -1, 4], ['dep', 4, 0], ['advmod', 4, 1], ['aux', 4, 2], ['nsubj', 4, 3], ['punct', 4, 5]],
                      'ner': ['O', 'O', 'O', 'O', 'O', 'O'], 
                      'tokens': ['Hello', 'how', 'are', 'you', 'doing', '?']},
                    {'parse': '(ROOT (S (NP (PRP I)) (VP (VBP am) (VP (VBG enjoying)))))',
                      'deps_basic': [['root', -1, 2], ['nsubj', 2, 0], ['aux', 2, 1]], 
                      'char_offsets': [[25, 26], [27, 29], [30, 38]], 
                      'normner': ['', '', ''],
                       'lemmas': ['I', 'be', 'enjoy'],
                        'pos': ['PRP', 'VBP', 'VBG'],
                         'deps_cc': [['root', -1, 2], ['nsubj', 2, 0], ['aux', 2, 1]], 
                         'ner': ['O', 'O', 'O'], 'tokens': ['I', 'am', 'enjoying']}]}

