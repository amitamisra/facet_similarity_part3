#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import json
import csv
from sys import intern
from stance.pmi.Pmi import Pmi
from collections import defaultdict
#from CMUTweetTagger import runtagger_parse as CMU_parse

class NgramStancePmiBuilder:

    def __init__(self, n: int, input_file: str, min_count=5, stance_no_stance=True, use_punctuation=False):
        """
        :param n:
        :param input_file: The semeval input file with tweets and stances
        :param min_count: The minimum count of an ngram before it is included
        :param stance_no_stance: True to do stance vs nostance, False to do Favor vs Against vs None
        :param use_punctuation: True to keep punctuation
        :return:
        """
        self.n = n
        self.pmi = {n: Pmi() for n in range(1, self.n + 1)}
        self.use_punctuation = use_punctuation
        if not self.use_punctuation:
            self.punctuation = string.punctuation
        # Change topic to topic_id, might throw an error if the topic is not valid
        # Make a dictionary of pmi calculating objects, one for each value of n
        self.input_file = input_file
        self.min_count = min_count
        self.stance_no_stance = stance_no_stance

    def _retrieve_tweets(self):
        """
        Retrieve texts from the database that are not in the other or unknown topics.
        :return: tuples of (topic, [posts])
        """
        with open(self.input_file, 'r') as f_in:
            reader = csv.reader(f_in)
            header = next(reader)
            results = list()
            if self.stance_no_stance:
                for row in reader:
                    if row[header.index("Stance")] != "NONE":
                        results.append(("STANCE", row[header.index("Tweet")]))
                    else:
                        results.append(("NONE", row[header.index("Tweet")]))
            else:
                results = [(row[header.index("Stance")], row[header.index("Tweet")]) for row in reader]

            return results

    def _get_ngrams(self, labeled_tweets: list) -> list:
        """
        Get ngrams from a tweet's text
        :param tweet:
        :return: A list of tuples that represent ngrams
        """
        labels = [tup[0] for tup in labeled_tweets]
        tweets = [tup[1] for tup in labeled_tweets]
        word_lists = list()
        for token_list in CMU_parse(tweets):
            words = list()
            for token, type, conf in token_list:
                if not self.use_punctuation:
                    if token not in self.punctuation:
                        words.append(intern(token))
                else:
                    words.append(intern(token))

            word_lists.append(words)

        results = list()
        for label, words in zip(labels, word_lists):
            ngram_dict = defaultdict(list)
            for i in range(0, len(words)):
                n = self.n if i + self.n < len(words) else len(words) - i
                for j in range(1, n + 1):
                    ngram_dict[j].append(tuple(intern(words[k]) for k in range(i, i + j)))
            results.append((label, ngram_dict))

        return results

    def _write_pmi(self, output_file: str):
        with open(output_file, 'w', encoding='utf-8') as f_out:
            header = ["order", "ngram", "topic", "pmi", "npmi"]
            writer = csv.writer(f_out)
            writer.writerow(header)

            for order in range(1, len(self.pmi) + 1):
                pmi_dict = self.pmi[order].calculate_pmi()
                for ngram, topic_dict in pmi_dict.items():
                    for topic in topic_dict.keys():
                        pmi, npmi = pmi_dict[ngram][topic]
                        # print("order: {0}, ngram: {1}, topic: {2}, pmi: {3}, npmi: {4}".format(
                        #     order, ngram, topic, pmi, npmi
                        # ))
                        row = [order, ngram, topic, pmi, npmi]
                        writer.writerow(row)

    def _tuple_to_str(self, tup):
        return intern(" ".join(tup))

    def build(self, output_file: str):
        labeled_tweet_tokens = self._get_ngrams(self._retrieve_tweets())
        for stance, ngram_dict in labeled_tweet_tokens:
            for n, ngrams in ngram_dict.items():
                self.pmi[n].feed([(self._tuple_to_str(ngram), stance) for ngram in ngrams])

        self._write_pmi(output_file)

if __name__ == "__main__":
    # NgramStancePmiBuilder(3, min_count=5,
    #                       input_file="/home/brian/git/stance/data/training/semeval2016-task6-trainingdata_balanced.csv",
    #                       stance_no_stance=True).build("/home/brian/git/stance/data/pmi/stance_none_pmi_ngrams.csv")
    NgramStancePmiBuilder(3, min_count=10,
                          input_file="/home/brian/git/stance/data/training/semeval2016-task6-trainingdata_balanced.csv",
                          stance_no_stance=True).build("/home/brian/git/stance/data/pmi/stance_none_pmi_ngrams.csv")
