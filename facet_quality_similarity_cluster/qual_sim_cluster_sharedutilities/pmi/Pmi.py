#!/usr/bin/env python
# -*- coding: utf-8 -*-
from _collections_abc import Iterable
from collections import Counter
from collections import defaultdict
from math import log
from sys import intern

class Pmi:
    """
    Class for calculating the pmi of outcomes x and y.
    Feed an arbitrary amount of input into the class using the feed method, and then return the pmi and npmi using
    the calculate_pmi method.
    """

    def __init__(self, min_count=5):
        self.total_instances = 0
        self.min_count = min_count
        self.cx = Counter()
        self.cy = Counter()
        self.cxy = dict()

    def _intern_items(self, x):
        """
        Intern a string or intern all string elements of a tuple or list
        :param x:
        :return:
        """
        if isinstance(x, tuple) or isinstance(x, list):
            return tuple((intern(x_elem) for x_elem in x))
        elif isinstance(x, str):
            return intern(x)
        else:
            return x

    def feed(self, input_sample: Iterable):
        """
        Feed input to the counts
        :param input_sample: Iterable with tuples of (instance of x, instance of y)
        :return:
        """
        for x, y in input_sample:
            x = self._intern_items(x)
            y = self._intern_items(y)
            self.total_instances += 1
            self.cx[x] += 1
            self.cy[y] += 1
            if x not in self.cxy:
                self.cxy[x] = Counter()
            self.cxy[x][y] += 1

    def yield_pmi(self):
        """
        Calculate pmi for all (x, y)
        :return: A dictionary of [x][y] = (pmi, normalized_pmi)
        """
        for x in self.cxy.keys():
            for y in self.cxy[x].keys():
                if self.cx[x] >= self.min_count:
                    pxy = self.cxy[x][y] / self.total_instances
                    # Because we expect (x, y) input tuples,
                    # self.total_instances = total_x_instances = total_y_instances therefore
                    # p(x) = c(x) / self.total_instances and p(y) = c(y) / self.total_instances
                    px = self.cx[x] / self.total_instances
                    py = self.cy[y] / self.total_instances
                    pmi = log(pxy / (px * py))
                    npmi = pmi / (-1 * log(pxy))
                    yield (x, y, pmi, npmi)
                    # if x not in results:
                    #     results[x] = dict()
                    # results[x][y] = (pmi, npmi)
        #
        # return results

if __name__ == "__main__":
    unigrams = Pmi(min_count=1)
    # unigrams.feed([("gun", "gun control"), ("foetus", "abortion"), ("death", "gun control"), ("death", "abortion"),
    #                ("danger", "gun control"), ("danger", "gun control"), ("danger", "abortion")])
    unigrams.feed([(("gun",), "gun control"), (("foetus",), "abortion"), (("death",), "gun control"), (("death",), "abortion")])

    pmis = unigrams.calculate_pmi()
    print(pmis)
    # for x in pmis.keys():
    #     for y in pmis[x].keys():
    #         print("Pmi for ({x}, {y}) = {pmi}, npmi = {npmi}".format(x=x, y=y, pmi=pmis[x][y][0], npmi=pmis[x][y][1]))
