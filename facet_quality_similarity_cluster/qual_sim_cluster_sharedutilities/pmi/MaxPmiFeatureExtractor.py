#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pmi.PmiTopicTagger import PmiTopicTagger


class MaxPmiFeatureExtractor:
    def __init__(self, order=3, normalized=True):
        """
        :param top_n_percent: The top n percent of pmi words to use from each topic in order to generate the pmi pool
        :param order: max ngram order. Will use n = [1, 2,..., order]
        :param normalized: use normalized pmi
        :return:
        """
        self.tagger = PmiTopicTagger(orders=[1, 2, 3], topics=["abortion", "climate change is a real concern",
                                                               "atheism", "Hillary Clinton", "feminist movement"])
        self.order = order
        self.normalized = normalized

    def featurize(self, ngram_list, topic) -> float:
        """
        :param tokens: The ngrams from Features.ngrams_from_rowdict
        :return: A float giving the max pmi ngram
        """
        pmi_list = [val for val in [self.tagger.tag_tuple(ngram, topic, self.normalized) for ngram in ngram_list]
                    if val is not None]
        return max(pmi_list) if len(pmi_list) != 0 else 0.0

