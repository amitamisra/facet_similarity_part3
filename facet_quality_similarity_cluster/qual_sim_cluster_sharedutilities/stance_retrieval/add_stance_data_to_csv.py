import os

import pandas

from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities.db.db_connect import setup_connection
from qual_sim_cluster_sharedutilities.stance_retrieval.stance_data import get_stance_info

_, sql_session = setup_connection()


def get_stance(row):
    global sql_session
    return get_stance_info(sql_session, 0.5, row["datasetId"], row["discussionId"], row["postId"])


with open(file_utilities.load_resource("scratch/dp_sents_wc-10-40_qp-gt-75_dict-words-gt-3_overlap-80_require-verb_quote-artifacts.csv")) as fh:
    df = pandas.read_csv(fh)
    stance_col_tuples = df.apply(get_stance, axis=1)
    df["self_annotated_stance"] = pandas.Series(map(lambda x: x[0], stance_col_tuples))
    df["topic_stance"] = pandas.Series(map(lambda x: x[1], stance_col_tuples))
    print(df)

with open(os.path.join(file_utilities.RESOURCE_DIR, "scratch/dp_filtered_with_stance.csv"), 'w') as fh:
    df.to_csv(fh)
