import os
import random
import time

import gensim

import numpy as np
from sklearn.manifold import TSNE
import matplotlib as mpl
mpl.use('PS')
import matplotlib.pyplot as plt
from qual_sim_cluster_sharedutilities.file_utilities import load_afs_feature_data, load_resource
from similarity.afs_w2v.train_similarity import w2v_concat


def run(num_figs):
    # Load java AFS data
    data = load_afs_feature_data()
    # Load w2v model
    w2v = gensim.models.Word2Vec.load_word2vec_format(load_resource("w2v/google_300.bin"), binary=True)
    print("Using word2vec model with {0} dimensional vectors".format(w2v.vector_size))

    for subset in ["train", "test"]:
        if subset != "train":
            continue
        for topic_ind, topic in enumerate(["GC", "GM", "DP"]):
            print("RESULTS FOR {0} {1} subset:".format(topic, subset))
            # features = afs_with_w2v(data[subset][topic], w2v)
            features = w2v_concat(data[subset][topic], w2v)
            print("Starting tsne training...")
            t = time.time()
            tsne = TSNE(n_components=2, random_state=0)
            np.set_printoptions(suppress=True)  # Don't debug like crazy
            t_points = tsne.fit_transform(features)
            print("Training took {0} seconds".format(time.time() - t))

            # plt.plot(*(zip(*t_points)), ls="", marker="o")
            # for label, x, y in zip(["test_{0}".format(i) for i in range(1, 11)], data[:, 0], data[:, 1]):
            #     plt.annotate(label, xy=(x, y), xytext=(-5, 5), textcoords='offset points', ha='right', va='bottom')

            for fig_num in range((topic_ind - 1) * num_figs, topic_ind * num_figs):
                plt.figure(fig_num, figsize=(50, 50))
                sample_indices = np.random.randint(t_points.shape[0], size=10)

                plt.plot(*zip(*t_points), ls="", marker="o")

                for s1, s2, x, y in zip(data[subset][topic]["meta"]["sentence_1"].values[sample_indices],
                                        data[subset][topic]["meta"]["sentence_2"].values[sample_indices],
                                        t_points[sample_indices, :][:, 0],
                                        t_points[sample_indices, :][:, 1]):
                    plt.annotate("{s1}\n{s2}".format(s1=s1, s2=s2),
                                 xy=(x, y),
                                 xytext=(-5, 5),
                                 textcoords="offset points",
                                 ha="right",
                                 va="bottom",
                                 size=6)

                out_dir = load_resource("scratch/tsne_plots")
                if not os.path.exists(out_dir):
                    os.makedirs(out_dir)
                plt.savefig(os.path.join(out_dir, "{0}_fig_{1}.png".format(topic,
                                                                           fig_num - ((topic_ind - 1) * num_figs))))


def mpl_test():
    random.seed(0)
    data = np.random.random((10, 2))
    for fig in range(10):
        plt.figure(fig)
        sample_indices = np.random.randint(10, size=2)
        print(sample_indices)

        plt.plot(*zip(*data), ls="", marker="o")

        for i, (x, y) in enumerate(data[sample_indices, :]):
            plt.annotate("test_{0}".format(i), xy=(x, y), xytext=(-5, 5), textcoords="offset points", ha="right",
                         va="bottom", size=6)

        # for label, x, y in zip(["test_{0}".format(i) for i in range(1, 11)], data[:, 0], data[:, 1]):
        #     plt.annotate(label, xy=(x, y), xytext=(-5, 5), textcoords='offset points', ha='right', va='bottom')

        out_dir = load_resource("scratch/tsne_plots")
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        plt.savefig(os.path.join(out_dir, "fig_{0}.png".format(fig)))
        # plt.show()


if __name__ == "__main__":
    plt.interactive(False)
    run(10)
    # mpl_test()
