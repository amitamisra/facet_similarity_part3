import os
import random
import time
import gensim
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from qual_sim_cluster_sharedutilities.file_utilities import load_afs_feature_data, load_resource
from similarity.afs_w2v.train_similarity import sent2vec


def w2v_representation(dataset, w2v):
    print("Doing ml experiments for concatenated w2v vectors")
    feats = list()
    labels = list()
    seen = set()

    def add(s):
        if s not in seen:
            v = sent2vec(s, w2v)
            feats.append(v)
            labels.append(s)
            seen.add(s)

    for s1, s2 in zip(dataset["meta"]["sentence_1"], dataset["meta"]["sentence_2"]):
        add(s1)
        add(s2)

    return np.array(labels), np.array(feats)


def run():
    fig = None
    picked = set()
    p_labels = dict()

    def onpick(event):
        this = event.artist
        if this.index in picked:
            # Remove the text
            # picked[this.index].remove()
            p_labels[this.index].set_visible(False)
            picked.remove(this.index)
        else:
            # Add the text
            p_labels[this.index].set_visible(True)

            # annotation = plt.annotate(labels[this.index],
            #                           xy=(this.get_xdata(), this.get_ydata()),
            #                           xytext=(-5, 5),
            #                           textcoords="offset points",
            #                           ha="right",
            #                           va="bottom",
            #                           size=6)
            picked.add(this.index)

        # Redraw to update
        fig.canvas.draw()

    # Load java AFS data
    data = load_afs_feature_data()
    # Load w2v model
    w2v = gensim.models.Word2Vec.load_word2vec_format(load_resource("w2v/google_300.bin"), binary=True)
    print("Using word2vec model with {0} dimensional vectors".format(w2v.vector_size))

    for subset in ["train", "test"]:
        if subset != "train":
            continue
        for topic_ind, topic in enumerate(["GC", "GM", "DP"]):
            labels, features = w2v_representation(data[subset][topic], w2v)

            for exag in [0.5 * i for i in range(8, 9)]:
                for lr in [100 * i for i in range(10, 11)]:
                    print("early exaggeration: {0}, learning rate: {1}".format(exag, lr))
                    fig = plt.figure(figsize=(10, 10))
                    print("Starting tsne training...")
                    t = time.time()
                    tsne = TSNE(n_components=2, random_state=0, early_exaggeration=exag, learning_rate=lr)
                    np.set_printoptions(suppress=True)  # Don't debug like crazy
                    t_points = tsne.fit_transform(features)
                    print("Training took {0} seconds".format(time.time() - t))

                    ax = fig.add_subplot(111)
                    ax.set_title("Click points to see their sentences")
                    for index, (label, x, y) in enumerate(zip(labels, t_points[:, 0], t_points[:, 1])):
                        pt, = ax.plot(x, y, ls="", marker="o", color="r", picker=7)
                        pt.index = index
                        p_label = plt.annotate(label,
                                               xy=(x, y),
                                               xytext=(-5, 5),
                                               textcoords="offset points",
                                               ha="right",
                                               va="bottom",
                                               size=6)
                        p_label.set_visible(False)
                        p_labels[pt.index] = p_label

                    fig.canvas.mpl_connect("pick_event", onpick)
                    plt.show()


if __name__ == "__main__":
    run()
