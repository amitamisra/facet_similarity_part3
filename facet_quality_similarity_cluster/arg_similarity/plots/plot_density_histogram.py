'''
Created on May 3, 2016
plots a histograms. 
@author: amita
'''
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

#----------------------------------- plots a histogram from values in plot file.
# Since we do not want individual bars for higher values, it uses numpy array to clip all
# values greater than 110 to 100. This helps to remove the ugly white space in the end

def plotgraph(df,col,plot_file):
    values=df[col].values
    plt.ioff()
    binsr = np.arange(0,120,10)
    print(binsr)
    n, bins, patches = plt.hist(np.clip(values, binsr[0], binsr[-1]),bins=binsr,color=["white"])
    plt.xlim([0, 120])
    plt.xticks([0,10,20,30,40,50,60,70,80,90,100,109],["0","10", "20","30","40","50","60","70", "80","90", "100","100+"])
    for bin_size, bin, patch in zip(n, bins, patches):
        if bin_size == max(n):
            patch.set_facecolor("grey")
                
    plt.text(30, 200, "Frequency Histogram",horizontalalignment='center')
    plt.xlabel("Word Count in a sentence with prediction score > 0.91")
    plt.ylabel("Number for sentences with prediction score > 0.91")
    plt.savefig(plot_file)
    plt.close()
    
    

if __name__ == '__main__':
    inputFile="/Users/amita/facet_similarity_part2/data/gc_dp_gm_gt55_sentences_wc_gtscore91.csv"
    plot_file="/Users/amita/facet_similarity_part2/data/gc_dp_gm_gt_91histogm.png"
    df_gt91=pd.read_csv(inputFile)
    plotgraph(df_gt91,"word_count_sentence",plot_file)