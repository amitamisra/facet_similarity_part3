'''
Created on Jul 18, 2016
plot word density wrt to AFS Quality
@author: amita
'''
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

#----------------------------------- plots a histogram from values in plot file.
# Since we do not want individual bars for higher values, it uses numpy array to clip all
# values greater than 110 to 100. This helps to remove the ugly white space in the end

def plotgraph(df,col1,col2,plot_file):
    values1=df[col1].values
    values2=df[col2].values
    plt.hist(values1,alpha=0.5,label="word_count sentence1",color="w",ls='--')
    plt.hist(values2,alpha=0.5,label="word_count sentence2",color="w",ls=":")
    plt.xlabel("Word Count")
    plt.ylabel("Frequency")
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plot_file)
    plt.close()
    
    
if __name__ == '__main__':
    inputFile="/Users/amita/facet_similarity_part2/data/guncontrol/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/mt1_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000.csv"
    plot_file="/Users/amita/facet_similarity_part2/data/guncontrol/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/mt1_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_histogm.png"
    df=pd.read_csv(inputFile)
    plotgraph(df,"word_count_1","word_count_2",plot_file)
