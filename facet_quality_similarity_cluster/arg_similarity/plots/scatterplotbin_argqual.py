'''
Created on May 14, 2016

@author: amita
'''
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from functools import partial

matplotlib.rcParams.update({'font.size': 8})
matplotlib.rcParams['xtick.labelsize'] = 8 
matplotlib.rcParams['ytick.labelsize'] = 8 
def getbinno(binstring):
    binno=binstring.find(";")
    return int(binstring[binno-1])

# if 
def yesscore(scale3count,turkcountyes_quality):
    if int(scale3count)>=turkcountyes_quality:
        return 1
    else:
        return 0    
def plotgraph(inputFile,x,y,plot_file,turkcountyes_quality):
    df=pd.read_csv(inputFile)
    binno=[]
    binscore=[]
    plt.ioff()
    grouped=df.groupby(x,sort=True)
    for name, group in grouped:
        if "4 <= Prediction < 0.91000" in name:
            print(name+"ignore")
            continue
        mapfunc = partial(yesscore, turkcountyes_quality=turkcountyes_quality)
        avgbinscore=sum(list(map(mapfunc,group[y].values)))/len(group[y].values)
        binscore.append(avgbinscore)
        binno.append( getbinno(name))
        print(name)
    plt.figure(figsize=(5,5))
    plt.scatter(binno,binscore)
    plt.xticks([0,1,2,3,4],["0.55-0.64","0.64-0.73","0.73-0.82", "0.82-0.91","0.91+"])
    #plt.yticks([0.4,0.5,0.6,0.7,0.8,0.9],["0.4","0.5","0.6","0.7","0.8","0.9"])
    #plt.text(0.6, 0.5, "Probability of getting an annotated argument")
    ##plt.text(0.6,0.46,"Argument Quality HIT with rescored model")
    plt.xlabel("Predicted scores bin range")
    plt.ylabel("Probabilty")
    plt.savefig(plot_file)
    plt.close()
    

if __name__ == '__main__':
    inputFile="/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/Results/mt1_split_dp_gt55_argQualScale.csv"
    plot_file="/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/Results/mt1_split_dp_gt55_argQualScale_plot.png"
    x="bin"
    y="scale3_Count"
    # number of turkers say yes/scale3 for DP, if 2 for DP then good quality argument
    turkcountyes_quality=2
    plotgraph(inputFile,x,y,plot_file,turkcountyes_quality)