library("ggplot2")
data.dir <- "/home/brian/nlp/naacl2016/graphs"
bot <- read.csv(paste(data.dir, "mt1_truescores_split_gc_gt55_argQuality_yesCount_gt3_combinations_sim_max-sent-inclusions-10_bottom200umbc.csv", sep="/"))
mid <- read.csv(paste(data.dir, "mt1_truescores_split_gc_gt55_argQuality_yesCount_gt3_combinations_sim_max-sent-inclusions-10_mid200umbc.csv", sep="/"))
top.2000 <- read.csv(paste("/home/brian/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/experiment_data/afs/", "mt1_truelabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_Trainfeatures_norm.csv", sep="/"))

ggplot(data=df, aes(x=df$UmbcSimilarity, y=df$regression_label)) + geom_point() + xlab("UMBC Similarity") + ylab("Annotated AFS")

ggplot() + 
  # Don't want the middle set
  # geom_point(aes(x=mid$similarity, y=mid$regression_label), shape=0) + 
  geom_point(aes(x=bot$similarity, y=bot$regression_label, shape=16)) +
  geom_point(aes(x=top.2000$similarity, y=top.2000$regression_label, shape=17)) +
  scale_shape_identity() + # Use numeric codes for shapes
  xlab("Umbc Similarity Score") +
  ylab("Annotated AFS") +
  theme_bw() +
  theme(legend.position="none") +
  theme(axis.title.y = element_text(colour="grey20",size=14,face="bold"),
        axis.text.x = element_text(colour="grey20",size=14,face="bold"),
        axis.text.y = element_text(colour="grey20",size=14,face="bold"),  
        axis.title.x = element_text(colour="grey20",size=14,face="bold"))  
        