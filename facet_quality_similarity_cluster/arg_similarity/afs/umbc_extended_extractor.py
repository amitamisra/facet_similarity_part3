import json
import os
import re
from multiprocessing import RLock
from time import sleep

from qual_sim_cluster_sharedutilities import file_utilities
from similarity.afs.feature_utils import flatten_annotation_to_tokens, transform_and_filter_word_pos, log_length_normalization
from similarity.afs.umbc_sim import UmbcSimilarity, UmbcPageDownException

UMBC_DIR = file_utilities.load_resource("umbc")
DISK_CACHE = os.path.join(UMBC_DIR, "topn_cache.txt")


def _read_dict():
    if not os.path.exists(DISK_CACHE):
        return dict()
    else:
        with open(DISK_CACHE, 'r') as fh:
            return json.load(fh)


class UmbcExtendedExtractor:
    NN = (re.compile(r"NN[PS]{0,2}"), "NN")
    VB = (re.compile(r"VB[DGNPZ]?"), "VB")
    JJ = (re.compile(r"JJ[RS]?"), "JJ")
    PRE_FILTERS = [NN, VB, JJ]
    POST_FILTERS = [NN, VB, JJ]
    # We can share this dictionary because updates are atomic
    # https://docs.python.org/3/faq/library.html#what-kinds-of-global-value-mutation-are-thread-safe
    # Although it's possible that something like:
    #     if key not in cache_dict:
    #         cache_dict[key] = x
    # might conflict with
    #     cache_dict[key] = y
    # and thus cache_dict[key] would have either x or y, this is not actually too important, because at most it results
    # in n_threads extra lookups for a word.
    cache_dict = None

    # Lock is for initializing and writing to disk only
    lock = RLock()
    if cache_dict is None:
        lock.acquire()
        try:
            # Have the lock, read the dictionary
            if cache_dict is None:
                cache_dict = _read_dict()
        finally:
            lock.release()

    def __init__(self, num_nn=5, num_vb=5, num_jj=3):
        self.n = {"NN": num_nn, "VB": num_vb, "JJ": num_jj}

    def extract(self, annotation_1, annotation_2):
        # Filters for specific POS only and simplifies them to a form Umbc wants
        p1 = self._pos_filter_and_simplify(
            # Does the standard lower case, stopword, and punctuation filtering
            transform_and_filter_word_pos(zip(flatten_annotation_to_tokens(annotation_1, "lemmas"),
                                              flatten_annotation_to_tokens(annotation_1, "pos"))),
            UmbcExtendedExtractor.PRE_FILTERS)
        p2 = self._pos_filter_and_simplify(
            transform_and_filter_word_pos(zip(flatten_annotation_to_tokens(annotation_2, "lemmas"),
                                              flatten_annotation_to_tokens(annotation_2, "pos"))),
            UmbcExtendedExtractor.PRE_FILTERS)

        success = False
        while not success:
            try:
                top_n_1 = self._get_n(p1)
                top_n_2 = self._get_n(p2)
                success = True
            except UmbcPageDownException:
                print("Caught UmbcPageDownException, sleeping for 30 seconds then trying again.")
                sleep(30)

        return {"UMBC_COMBINED_AUG_OVERLAP_NORMALIZED": log_length_normalization(len(top_n_1 & top_n_2), annotation_1,
                                                                                 annotation_2)}

    def _get_n(self, pairs):
        added_words = list()
        for word_pos in pairs:
            n = self.n[word_pos[1]]
            cache_key = "{n}_{word}_{pos}".format(n=n, word=word_pos[0], pos=word_pos[1])
            if cache_key not in UmbcExtendedExtractor.cache_dict:
                top_n = UmbcSimilarity.get_top_n(word_pos[0], word_pos[1], n, "concept", "webbase")
                UmbcExtendedExtractor.cache_dict[cache_key] = top_n
            else:
                top_n = UmbcExtendedExtractor.cache_dict[cache_key]

            added_words += top_n
            added_words.append(word_pos)

        return set(self._pos_filter_and_simplify(added_words, UmbcExtendedExtractor.POST_FILTERS))

    def _pos_filter_and_simplify(self, tups, filters):
        results = list()
        for tup in tups:
            for f in filters:
                if f[0].match(tup[1]) is not None:
                    results.append((tup[0], f[1]))

        return results

    @staticmethod
    def write_dict():
        UmbcExtendedExtractor.lock.acquire()
        try:
            # Have the lock, save the dictionary
            if not os.path.exists(UMBC_DIR):
                os.makedirs(UMBC_DIR)
            with open(DISK_CACHE, 'w') as fh:
                json.dump(UmbcExtendedExtractor.cache_dict, fh)
        finally:
            UmbcExtendedExtractor.lock.release()
