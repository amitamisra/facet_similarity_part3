from collections import Counter

import numpy as np
from scipy import spatial

from arg_similarity.afs.feature_utils import build_ngrams, log_length_normalization, normalized_dict_overlap


class NgramExtractor:
    def __init__(self, order: int, tokens_key: str):
        """
        :param order: The n in ngram
        :param tokens_key: The key to get the tokens from each sentence dictionary, e.g. "tokens", "lemmas"
        :return:
        """
        self.order = order
        self.tokens_key = tokens_key

    def extract(self, annotation1, annotation2):
        """

        :param annotation1:
        :param annotation2:
        :return:
        """
        ngrams1 = Counter(build_ngrams(annotation1, self.tokens_key, self.order))
        ngrams2 = Counter(build_ngrams(annotation2, self.tokens_key, self.order))
        all_ngrams = list(set(ngrams1.keys()) | set(ngrams2.keys()))

        def make_vector(ngrams):
            vec = np.zeros(len(all_ngrams))
            for index, _ngram in enumerate(all_ngrams):
                # print("index: {0}, ngram: {1}".format(index, _ngram))
                vec[index] = ngrams[_ngram]
            return vec

        # spatial.distance.cosine computes the distance, not the similarity, so do 1 - dist
        cosine = 1.0 - spatial.distance.cosine(make_vector(ngrams1), make_vector(ngrams2))
        overlap_scores = normalized_dict_overlap(ngrams1, ngrams2)
        normalized_overlap = log_length_normalization(sum(overlap_scores.values()), annotation1, annotation2)

        return {"NGramCosine": cosine,
                "NGramIntersectionSizeNormalized": normalized_overlap}
