import os
import unittest

from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities.stanford_corenlp_pywrapper.sockwrap import CoreNLP
from similarity.afs.feature_utils import build_ngrams
from similarity.afs.liwc_dependency_extractor import LiwcDependencyExtractor
from similarity.afs.liwc_extractor import LiwcExtractor
from similarity.afs.ngram_extractor import NgramExtractor
from similarity.afs.rouge import rouge
from similarity.afs.umbc_extended_extractor import UmbcExtendedExtractor
from similarity.afs.umbc_sim import UmbcSimilarity, UmbcPageDownException


class TestAfs(unittest.TestCase):
    def setUp(self):
        self.test1 = "The quick brown fox jumps over the lazy dog."
        self.test2 = "The slow blue cat jumps languidly over a cute dog."
        self.test_parse1 = {'sentences': [{'char_offsets': [[0, 3], [4, 9], [10, 15], [16, 19], [20, 25], [26, 30],
                                                            [31, 34], [35, 39], [40, 43], [43, 44]],
                                           'parse': '(ROOT (S (NP (DT The) (JJ quick) (JJ brown) (NN fox)) '
                                                    '(VP (VBZ jumps) (PP (IN over) (NP (DT the) (JJ lazy) '
                                                    '(NN dog)))) (. .)))',
                                           'normner': ['', '', '', '', '', '', '', '', '', ''],
                                           'lemmas': ['the', 'quick', 'brown', 'fox', 'jump', 'over', 'the', 'lazy',
                                                      'dog', '.'],
                                           'ner': ['O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'],
                                           'deps_cc': [['root', -1, 4], ['det', 3, 0], ['amod', 3, 1], ['amod', 3, 2],
                                                       ['nsubj', 4, 3], ['nmod:over', 4, 8], ['punct', 4, 9],
                                                       ['case', 8, 5], ['det', 8, 6], ['amod', 8, 7]],
                                           'tokens': ['The', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy',
                                                      'dog', '.'],
                                           'deps_basic': [['root', -1, 4], ['det', 3, 0], ['amod', 3, 1],
                                                          ['amod', 3, 2], ['nsubj', 4, 3], ['nmod', 4, 8],
                                                          ['punct', 4, 9], ['case', 8, 5], ['det', 8, 6],
                                                          ['amod', 8, 7]],
                                           'pos': ['DT', 'JJ', 'JJ', 'NN', 'VBZ', 'IN', 'DT', 'JJ', 'NN', '.']}]}

        self.test_parse2 = {'sentences': [{'char_offsets': [[45, 48], [49, 53], [54, 58], [59, 62], [63, 68],
                                                            [69, 78], [79, 83], [84, 85], [86, 90], [91, 94],
                                                            [94, 95]],
                                           'parse': '(ROOT (S (NP (DT The) (JJ slow) (JJ blue) (NN cat)) '
                                                    '(VP (VBZ jumps) (ADVP (RB languidly)) (PP (IN over) '
                                                    '(NP (DT a) (JJ cute) (NN dog)))) (. .)))',
                                           'normner': ['', '', '', '', '', '', '', '', '', '', ''],
                                           'lemmas': ['the', 'slow', 'blue', 'cat', 'jump', 'languidly', 'over', 'a',
                                                      'cute', 'dog', '.'],
                                           'ner': ['O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'],
                                           'deps_cc': [['root', -1, 4], ['amod', 3, 2], ['det', 3, 0],
                                                       ['amod', 3, 1], ['nsubj', 4, 3], ['advmod', 4, 5],
                                                       ['nmod:over', 4, 9], ['punct', 4, 10], ['case', 9, 6],
                                                       ['det', 9, 7], ['amod', 9, 8]],
                                           'tokens': ['The', 'slow', 'blue', 'cat', 'jumps', 'languidly', 'over',
                                                      'a', 'cute', 'dog', '.'],
                                           'deps_basic': [['root', -1, 4], ['amod', 3, 2], ['det', 3, 0],
                                                          ['amod', 3, 1], ['nsubj', 4, 3], ['advmod', 4, 5],
                                                          ['nmod', 4, 9], ['punct', 4, 10], ['case', 9, 6],
                                                          ['det', 9, 7], ['amod', 9, 8]],
                                           'pos': ['DT', 'JJ', 'JJ', 'NN', 'VBZ', 'RB', 'IN', 'DT', 'JJ', 'NN',
                                                   '.']}]}

    def test_srparse(self):
        corenlp_path = file_utilities.load_resource("stanford-corenlp-full-2015-12-09")
        pos_tagger_path = file_utilities.load_resource("stanford-postagger-2015-12-09")
        srparser_path = file_utilities.load_resource("stanford-srparser-2014-10-23-models.jar")
        if not os.path.exists(corenlp_path):
            self.fail("\n\tMissing corenlp. Please download from "
                      "http://nlp.stanford.edu/software/stanford-corenlp-full-2015-12-09.zip\n"
                      "\tAfter downloading, uznip the file in resources")
        if not os.path.exists(pos_tagger_path):
            self.fail("\n\tMissing pos tagger models. Please download from "
                      "http://nlp.stanford.edu/software/stanford-postagger-2015-12-09.zip\n"
                      "\tAfter downloading, uznip the file in resources")
        if not os.path.exists(srparser_path):
            self.fail("\n\tMissing shift-reduce parser models. Please download models from "
                      "http://nlp.stanford.edu/software/stanford-srparser-2014-10-23-models.jar.\n"
                      "\tAfter Downloading, place the models jar file in resources")
        srparse_config = file_utilities.load_resource("config/srparse.properties")
        try:
            parser = CoreNLP(configfile=srparse_config,
                             corenlp_jars=[os.path.join(corenlp_path, "*"),
                                           os.path.join(pos_tagger_path, "*"),
                                           srparser_path])
        except Exception as e:
            self.fail("Failed to load parser: {0}".format(str(e)))

        # If we're still going, we successfully loaded the parser
        parsed_doc = parser.parse_doc("The quick brown fox jumps over the lazy dog. "
                                      "The slow blue cat jumps languidly over a cute dog.")

        # Check a few basic things just to make sure everything looks like it's working correctly.
        # Correct number of sentences
        self.assertEqual(len(parsed_doc["sentences"]), 2)
        # Correct number of tokens and lemmas
        self.assertEqual(len(parsed_doc["sentences"][0]["lemmas"]), 10)
        # Correctly marks brown as an adjectival modifier
        self.assertEqual(parsed_doc["sentences"][0]["deps_cc"][3][0], "amod")

    def test_ngram_construction(self):
        ngrams = build_ngrams(self.test_parse1, "tokens", 3)
        self.assertTrue(("quick", "brown", "fox") in ngrams)
        self.assertFalse(("dog", ".") in ngrams)
        self.assertFalse(("The",) in ngrams)
        self.assertFalse(("the",) in ngrams)
        self.assertFalse((".",) in ngrams)

    def test_ngram_extraction(self):
        extractor = NgramExtractor(3, "lemmas")
        feats = extractor.extract(self.test_parse1, self.test_parse2)
        self.assertTrue("NGramCosine" in feats)
        self.assertTrue("NGramIntersectionSizeNormalized" in feats)
        cosine = feats["NGramCosine"]
        intersection = feats["NGramIntersectionSizeNormalized"]
        self.assertTrue(0.0 < cosine < 1.0)
        self.assertTrue(0.0 < intersection < 1.0)
        self.assertTrue(cosine - 0.121 < 0.001)
        self.assertTrue(intersection - 0.370 < 0.001)

    def test_rouge(self):
        scores = rouge(self.test1, self.test2)
        self.assertEqual(scores["rouge_l_f_score"], 0.83077)

    def test_liwc(self):
        extractor = LiwcExtractor()
        scores = extractor.extract(self.test_parse1, self.test_parse2)
        self.assertTrue(scores["motion"] - 0.185 < 0.001)

    def test_umbc(self):
        sim = UmbcSimilarity.get_sim(self.test1, self.test2)
        self.assertTrue(sim - 0.732 < 0.001)

    def test_umbc_top_n(self):
        try:
            top_5 = UmbcSimilarity.get_top_n("cat", "NN", 5, "concept", "webbase")
            self.assertEqual(len(top_5), 5)
        except UmbcPageDownException as e:
            self.longMessage("Check umbc_top_n format: {0}".format(str(e)))

    def test_umbc_extended_extractor(self):
        extractor = UmbcExtendedExtractor()
        feats = extractor.extract(self.test_parse1, self.test_parse2)
        self.assertEqual(len(feats), 1)
        self.assertLess(feats["UMBC_COMBINED_AUG_OVERLAP_NORMALIZED"] - 2.781, 0.001)

    def test_liwc_deps(self):
        extractor = LiwcDependencyExtractor("gov")
        feats = extractor.extract(self.test_parse1, self.test_parse2)
        self.assertEqual(len(feats), 2)
        self.assertLess(feats["liwc_dep_overlap_norm"] - 0.741, 0.001)
        self.assertLess(feats["liwc_simplified_dep_overlap_norm"] - 0.741, 0.001)


if __name__ == '__main__':
    unittest.main()
