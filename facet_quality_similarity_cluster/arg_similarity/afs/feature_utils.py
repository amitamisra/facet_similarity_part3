import re
from math import log2

from qual_sim_cluster_sharedutilities import file_utilities


def build_ngrams(annotation, tokens_key, order):
    """
    Build all ngrams from an annotation dictionary
    :param annotation: The annotation dictionary from stanford parser
    :param tokens_key: The dictionary key in each sentence dictionary that gives tokens, e.g. "lemmas" or "tokens"
    :param order: The n in ngram
    :return: A list of all ngrams made within each sentence in all sentences
    """
    ngrams = list()
    for sentence in annotation["sentences"]:
        ngrams += ngram_combinations(sentence[tokens_key], order)

    return ngrams


not_punct_re = re.compile(r".*[a-zA-Z0-9].*")
stopwords = set()
with open(file_utilities.load_resource("stopwords-full-list.txt")) as fh:
    for line in fh:
        stopwords.add(line.strip())


def punctuation(token: str):
    return not_punct_re.match(token) is not None


def ngram_combinations(tokens, order):
    """
    Make ngram style combinations from the tokens
    :param tokens: A list of units to make into ngrams
    :param order: The n in ngram
    :return: A list of tuples
    """
    results = list()
    tokens = transform_and_filter_tokens(tokens)
    for i in range(0, len(tokens)):
        n = order if i + order < len(tokens) else len(tokens) - i
        for j in range(1, n + 1):
            results.append(tuple(tokens[k] for k in range(i, i + j)))

    return results


def stopword(token: str):
    return token not in stopwords


def transform_and_filter(tokens, get_filterable=lambda x: x, transformer=lambda x: x):
    results = list()
    for x in tokens:
        x_transformed = transformer(x)
        filterable = get_filterable(x_transformed)
        if stopword(filterable) and punctuation(filterable):
            results.append(x_transformed)
    return results


def transform_and_filter_tokens(tokens):
    return transform_and_filter(tokens, transformer=lambda x: x.lower())


def transform_and_filter_word_pos(word_pos):
    return transform_and_filter(word_pos, get_filterable=lambda x: x[0], transformer=lambda x: (x[0].lower(), x[1]))


def sentence_length(sentence):
    return len(transform_and_filter_tokens(sentence["tokens"]))


def text_length(annotation):
    length = 0
    for sentence in annotation["sentences"]:
        length += sentence_length(sentence)

    return length


def flatten_annotation_to_tokens(annotation, tokens_key):
    """
    Flattens an annotation into a list of tokens
    :param annotation:
    :param tokens_key: how to retrieve tokens from each sentence, e.g. "lemmas", "tokens"
    :return: A list of tokens
    """
    return [token for sentence in annotation["sentences"] for token in sentence[tokens_key]]


def log_length_normalization(value, annotation1, annotation2):
    """
    Normalizes a feature value based on the summed log length of both annotations
    :param value: Value to normalize
    :param annotation1: dictionary annotation
    :param annotation2: dictionary annotation
    :return: normalized feature value
    """
    norm_factor = log2(text_length(annotation1)) + log2(text_length(annotation2))
    if norm_factor == 0:
        return value
    else:
        return value / norm_factor


def normalized_dict_overlap(d1, d2, normalization_function=lambda x: x):
    """
    Calculate the normalized overlap between two dictionary-like items
    :param d1: dict-like object 1
    :param d2: dict-like object 2
    :param normalization_function: A function accepting a single value to normalize, default does no normalization
    :return: A dictionary of normalized overlap scores stored by key
    """
    results = dict()
    all_keys = set(d1.keys()) & set(d2.keys())
    for key in all_keys:
        if key in d1 and key in d2:
            # Overlapping key
            results[key] = normalization_function(min(d1[key], d2[key]))

    return results


class DependencyAlignmentException(Exception):
    pass


def annotation_to_dep_pairs(annotation, tokens_key):
    """
    Convert an annotation dictionary to a list of dependency pair tuples
    :param annotation:
    :param tokens_key: A key to retrieve tokens from each sentence, e.g. "tokens", "lemmas"
    :return: A list of dependency pair tuples, e.g. [(dog, amod, lazy), ...]
    """
    results = list()
    for sentence in annotation["sentences"]:
        tokens = sentence[tokens_key]
        deps = sentence["deps_cc"]
        for dep in deps:
            assert(len(dep) == 3)
            if dep[1] != -1 and not (0 <= dep[1] <= len(tokens)):
                raise DependencyAlignmentException()
            if dep[2] != -1 and not (0 <= dep[2] <= len(tokens)):
                raise DependencyAlignmentException()
            gov_token = tokens[dep[1]]
            dep_token = tokens[dep[2]]
            if gov_token and dep_token:
                # If not None and not empty strings (not sure this is possible, but just in case)
                results.append((gov_token, dep[0], dep_token))

    return results
