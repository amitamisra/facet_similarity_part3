'''
Created on May 24, 2016

@author: amita
'''
from unidecode import unidecode
from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities import stanfordparser_utility
from qual_sim_cluster_sharedutilities.stanford_corenlp_pywrapper.sockwrap import CoreNLP
from arg_similarity.afs.feature_utils import build_ngrams
from arg_similarity.afs.liwc_dependency_extractor import LiwcDependencyExtractor
from arg_similarity.afs.liwc_extractor import LiwcExtractor
from arg_similarity.afs.ngram_extractor import NgramExtractor
from arg_similarity.afs.rouge import rouge
from arg_similarity.afs.umbc_sim import UmbcSimilarity
import os 
import pandas as pd




def buildafsfeatures(sentence2,sentence1,parser):
   
    parsed_doc1 = parser.parse_doc(sentence1)
    parsed_doc2 = parser.parse_doc(sentence2)
    print (parsed_doc1)
    extractor = NgramExtractor(3, "lemmas")
    Ngramfeats = extractor.extract(parsed_doc1, parsed_doc2)
    rougescores = rouge(sentence1, sentence2)
    sim = UmbcSimilarity.get_sim(sentence1, sentence2)
    extractor = LiwcExtractor()
    liwcscores = extractor.extract(parsed_doc1, parsed_doc2)
    liwc_depextractor = LiwcDependencyExtractor("gov")
    liwc_depfeats = extractor.extract(parsed_doc1, parsed_doc2)
    
    
def parse_file(x):
    print(x["uniqueRowNo_2"])
    parsed_doc1=parser.parse_doc(x["sentence_1"])
    parsed_doc2=parser.parse_doc(x["sentence_2"])
    print(parsed_doc1)
    print(parsed_doc2)    
    
if __name__ == '__main__':
    #parser=stanfordparser_utility.getparse()
    input_file="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/experiment_data/afs/gc_test_corenlp.csv"
    #df=pd.read_csv(input_file)
    sentence1="Common sense gun control laws the gun haters want will only affect the law abiding citizens, not the criminals."
    sentence2="Notice not one of the common sense gun control laws gun hater pilot is demanding is to punish dangerous criminals by keeping them in prison to keep guns away from them!"
    #parsed_doc1=parser.parse_doc(sentence1)
    #print(parsed_doc1)
    scores=rouge(sentence1, sentence2)
    #parsed_doc1=parser.parse_doc("America is 1 in gun ownership it tanks only 107th in homicide rates.")
    print(scores)
    #print(parsed_doc1)
    #df.apply(parse_file,axis=1)
    #buildafsfeatures(sentence2,sentence1,parser)