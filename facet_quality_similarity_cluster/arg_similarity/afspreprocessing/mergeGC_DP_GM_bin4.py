'''
Created on Apr 28, 2016
merge argument quality for gp,gc,dp for gt>.55 and > 0.91(bin4) and plot word distribution for score > 0.91
@author: amita
'''

import pandas as pd
import configparser,os

#concatenates a list of csvs, all  have same header
def concatenatecsvs(csvlist, separator,encod):
    frames=[]
    for csv in csvlist:
        df=pd.read_csv(csv, sep=separator, encoding=encod)
        frames.append(df)
    result=pd.concat(frames)
    return result

def run(section):
    args= readCongigFile(section)
    inputlist= args[0]
    outputcsv= args[1]
    execute(inputlist,outputcsv)


def readCongigFile(section):
    
    config = configparser.ConfigParser()
    config_file = os.path.join(os.path.dirname(__file__), 'mergeGC_DP_GM_bin4_Config.ini')
    config.read(config_file, encoding='utf-8')
    input_file_list = config.get(section, 'input-file-list').split(",")
    output_file = config.get(section, 'output-file')
    #plot_file=config.get(section, 'plot-file')
    arguments=(input_file_list,output_file)
    return  (arguments)   


def execute(inputlist,outputcsv):
    df=concatenatecsvs(inputlist, ",", "utf-8")
    df_gt91=df.loc[(df['Prediction'] ) > 0.91]
    df.to_csv(outputcsv,index=False)
    df_gt91.to_csv(outputcsv[:-4]+"_gtscore91.csv",index=False)
   
    

if __name__ == '__main__':
    section="mergetopics"
    run(section)