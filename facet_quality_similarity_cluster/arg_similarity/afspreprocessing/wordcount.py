'''
Created on Jul 4, 2015
add word count using nltk tokenization  field to compare word distribution wrt prediction score
@author: amita
'''
from nltk import word_tokenize
import codecs, csv,  configparser, os
import copy
def readCsv(inputcsv):   
        inputfile = codecs.open(inputcsv,'r',encoding='utf-8') 
        result = list(csv.DictReader(inputfile))
        return result 

def writeCsv(outputcsv, rowdicts):
            fieldnames=rowdicts[0].keys()
            restval=""
            extrasaction="ignore"
            dialect="excel"
            outputfile = codecs.open(outputcsv ,'w',encoding='utf-8' )
            csv_writer = csv.DictWriter(outputfile, fieldnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC)
            csv_writer.writeheader()
            csv_writer.writerows(rowdicts) 
            outputfile.close()
        
def runGC(section):
    args= readCongigFile(section)
    inputcsv= args[0]
    outputcsv= args[1]
    field=args[2]
    execute(inputcsv,outputcsv, field)

def readCongigFile(section):
    
    config = configparser.ConfigParser()
    config_file = os.path.join(os.path.dirname(__file__), 'wordcount_Config.ini')
    config.read(config_file, encoding='utf-8')
    input_file = config.get(section, 'input-file')
    output_file = config.get(section, 'output-file')
    field_name= config.get(section, 'field-name')
    arguments=(input_file,output_file,field_name)
    return  (arguments)   

def execute(inputcsv, outputcsv,field):
    AllRows=readCsv(inputcsv)
    NewRows=[]
    for row in AllRows:
        newrow= copy.deepcopy(row)
        text= row[field]
        tokens=word_tokenize(text)
        word_count=len(tokens)
        strlen=len(text)
        newrow["word_count_"+ field] = word_count
        newrow["character_count_"+ field] = strlen
        newrow["average_wordlength"]=float(strlen/word_count)
        NewRows.append(newrow)
        
    writeCsv(outputcsv, NewRows)    
        
        
     
if __name__ == '__main__':
    section="GC5"
    runGC(section)
    section="DP"
    runGC(section)
    section="GM"
    runGC(section)
    
    
    
    