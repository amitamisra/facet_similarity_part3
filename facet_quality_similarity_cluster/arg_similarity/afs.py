"""
This module provides functions for training and testing AFS.
Two options for training include SVR and GP. More can be easily added.
Training via pipeline.
"""


import pickle
import numpy as np
import sklearn
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
from sklearn.pipeline import Pipeline
from sklearn import svm
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.grid_search import GridSearchCV
from sklearn.gaussian_process import GaussianProcess
from sklearn.cross_validation import cross_val_score
import pandas as pd
from sklearn import linear_model
from sklearn.metrics import r2_score
from sklearn import ensemble


default_pipeline = Pipeline([('scale', preprocessing.StandardScaler()), ('filter', SelectKBest(f_regression, k=5)),
                             ('model', svm.SVR(kernel='linear'))])

default_param_grid = {'model__kernel': ('linear', 'rbf'),
                      'model__C': (1,10,100,1000),
                      'model__gamma': ('auto', 0.1, 1, 10, 100),
                      'filter__k': (5, 10, 20, 40)}


def afs_train(X, y, pipeline = default_pipeline, param_grid = default_param_grid ):
    grid_search = GridSearchCV(pipeline, param_grid, n_jobs = 4)
    grid_search.fit(X, y)
    best_model = grid_search.best_estimator_
    print ("Cross validation MAE:", cross_val_score(best_model, X, y, scoring='mean_absolute_error'))
    print ("Cross validation MSE:", cross_val_score(best_model, X, y, scoring='mean_squared_error'))
    print ("Cross validation r2:", cross_val_score(best_model, X, y, scoring='r2'))
    return best_model


def afs_test(X, y, pipeline, cross_val = False):
    if cross_val:
        print ("Cross validation MAE:", cross_val_score(pipeline, X, y, scoring='mean_absolute_error'))
        print ("Cross validation MSE:", cross_val_score(pipeline, X, y, scoring='mean_squared_error'))
        print ("Cross validation R2:", cross_val_score(pipeline, X, y, scoring='r2'))
    pred = pipeline.predict(X)
    print ("Test MAE:", mean_absolute_error(y, pred), pow(mean_absolute_error(y, pred), 1/2))
    print ("Test MSE:", mean_squared_error(y, pred), pow(mean_squared_error(y, pred), 1/2))
    print ("Test R2:", r2_score(y, pred), pow(r2_score(y, pred), 1/2))


if __name__ == "main":
    # Load training data
    X_train = np.load("../resources/data/afs/arrays/gc_X_train.npy")
    Y_train = np.load("../resources/data/afs/arrays/gc_Y_train.npy")
    print (X_train.shape, Y_train.shape)
    # Load test data
    X_test = np.load("../resources/data/afs/arrays/gc_X_test.npy")
    Y_test = np.load("../resources/data/afs/arrays/gc_Y_test.npy")
    print (X_test.shape, Y_test.shape)
    # Regular Linear model
    pipeline = Pipeline([('scale', preprocessing.StandardScaler()), ('filter', SelectKBest(f_regression, k=10)),
                         ('model', linear_model.LinearRegression())])
    param_grid = {'filter__k': (5, 10, 20, 40)}
    model_linear = afs_train(X_train, Y_train, pipeline, param_grid)
    afs_test(X_test, Y_test, model_linear)
