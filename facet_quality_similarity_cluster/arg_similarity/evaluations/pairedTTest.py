'''
Created on Apr 14, 2016
Take a sequence of pair of files as input and run pairedTTest on predictedscores
@author: amita
'''

import configparser, os
import  pandas as pd
from scipy.stats import ttest_rel
from scipy.stats import ttest_ind
from scipy import stats, math
PROJECT_DIR = os.path.dirname(os.path.abspath((os.path.join(os.path.dirname(__file__), ".."))))
RESOURCE_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "resources"))

def pairedTTest(param_dict):
    numrows= int(param_dict["numrows"])
    predict_col=param_dict["predict_col"]
    input1=param_dict["input1"]
    print(input1)
    df1=pd.read_csv(input1,na_filter=False,nrows=numrows)
    predicted1=df1[predict_col].tolist()
    predicted1=[math.sqrt(abs(x)) for x in  predicted1 ]
    
    input2=param_dict["input2"]
    df2=pd.read_csv(input2,na_filter=False,nrows=numrows)
    predicted2=df2[predict_col].tolist()  
    predicted2=[math.sqrt(abs(x)) for x in  predicted2 ]
    assert(len(predicted1)==len(predicted2))
    pairedTTest=ttest_rel(predicted1,predicted2) 
    return pairedTTest


def addresult(ttest,param_dict,all_list):
    result_dict={}
    input1=param_dict["input1"]
    input2=param_dict["input2"]
    result_dict["input1"]=input1
    result_dict["input2"]=input2
    result_dict["feature"]=os.path.basename(input1)+"_"+os.path.basename(input2)
    result_dict["pairedTTest"+param_dict["predict_col"]]=ttest
    all_list.append(result_dict)

def run(section): 
    config = configparser.ConfigParser()
    regressioninput=os.path.abspath(os.path.join(RESOURCE_DIR, "configfiles","pairedTTest.ini"))
    config.read(regressioninput)
    inputdata=config.get(section,"csv_with_file_pairs_list")
    resultCSVFile=config.get(section,"resultCSVFile")
    inputdata_df=pd.read_csv(inputdata,na_filter=False)
    all_list=[]
    if os.path.exists(resultCSVFile):
            os.remove(resultCSVFile)
    for index, row in inputdata_df.iterrows():
        param_dict={}
        param_dict["input1"]=row["input1"]
        param_dict["input2"]=row["input2"]
        param_dict["predict_col"]=row["predict_col"]
        param_dict["numrows"]=row["numrows"]
        ttest= pairedTTest(param_dict)
        addresult(ttest,param_dict,all_list)
        
    df=pd.DataFrame(all_list)
    df.to_csv(resultCSVFile,index=False)     
if __name__ == '__main__':
    section="GC"
    run(section)
    section="DP"
    run(section)
    section="GM"
    run(section)
    
    
    
    
    #actual2=df2["Y_actual"].tolist() 
    #error2=[abs(x - y) for x, y in zip(predicted2, actual2)]
    #errorsq2=[math.sqrt(math.pow(x,2)) for x in  error2 ]
    #actual1=df1["Y_actual"].tolist()
    #error1=[abs(x - y) for x, y in zip(predicted1, actual1)]
    #errorsq1=[math.sqrt(math.pow(x,2)) for x in  error1 ]