'''
Created on May 16, 2016

@author: amita
'''
import pandas as pd
import file_utilities,os
from numpy.ma import corrcoef
import numpy as np
from sklearn.feature_extraction import DictVectorizer

def afs_feature_by_key(dataset, feature):
    print("Taking AFS features that equals: \"{0}\"".format(feature))
    vectorizer = DictVectorizer()
    rouge_feats = [{key: val for key, val in feature_dict.items() if key==feature}
                   for feature_dict in dataset["features"]]
    rouge_vecs = vectorizer.fit_transform(rouge_feats).todense()
    assert(len(rouge_feats) == len(dataset["features"]))
    return rouge_vecs

def corrleation(featurelist,regression_label,df,topic):
    labelvalues=df[regression_label]
    for feature in featurelist:
        rouge_mat=afs_feature_by_key(df,feature)
        rouge_scores=np.array(rouge_mat).flatten()
        corr=corrcoef(rouge_scores,labelvalues)
        print (corr)
        print(topic)
    

if __name__ == '__main__':
    featurelist=["rouge_w_1.2_f_score","rouge_l_f_score","rouge_2_f_score","rouge_4_f_score","rouge_su*_f_score","rouge_1_f_score","rouge_3_f_score","rouge_s*_f_score"]
    df=file_utilities.load_afs_feature_data()
    regression_label="true_label"
    for topic in ["GC", "GM", "DP"]:
            print("correlations for topic{0}".format(topic))
            traindf=df["train"][topic]
            corrleation(featurelist,regression_label,traindf,topic)