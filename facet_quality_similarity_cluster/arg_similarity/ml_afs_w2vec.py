import os
import string

import gensim
import nltk
import numpy as np
import pandas
from nltk.corpus import stopwords
from numpy.ma import corrcoef
from scipy import spatial
from sklearn import cross_validation, clone, preprocessing
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.metrics import mean_squared_error
from sklearn.pipeline import Pipeline
from sklearn.svm import SVR

from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities.file_utilities import load_afs_feature_data, load_resource


def calc_error(y_pred, y):
    rmse = np.sqrt(mean_squared_error(y, y_pred))
    r = corrcoef(y, y_pred)[0, 1]
    rrse = rmse / np.sqrt(mean_squared_error(y, np.repeat(y.mean(), len(y))))

    return r, rmse, rrse


def print_error(y_pred, y):
    r, rmse, rrse = calc_error(y_pred, y)
    print("\tError:")
    print("\t\tAverage r: {0}".format(r))
    print("\t\tAverage RMSE: {0:.2f}".format(rmse))
    print("\t\tAverage RRSE: {0:.2f}".format(rrse))


def print_params(params):
    print("\t\tbest params:")
    for param, val in params.items():
        print("\t\t\t{0}: {1}".format(param, val))


def _cross_val(clf, x, y):
    y_pred = cross_validation.cross_val_predict(clf, x, y, cv=10)

    return y_pred


def cross_val(clf, x, y):
    """
    Do cross validation over 10 folds
    :param clf:
    :param x:
    :param y:
    :return: Return predictions and a best parameters dictionary. If clf is not a GridSearchCV, then the best parameters
    dict will be empty.
    """
    kfold = cross_validation.KFold(len(x), n_folds=10)
    predictions = list()
    best_params_by_fold = list()
    for fold_num, indices in enumerate(kfold):
        train_indices = indices[0]
        test_indices = indices[1]
        _clf = clone(clf)
        _clf.fit(x[train_indices], y[train_indices])
        test_pred = _clf.predict(x[test_indices])

        assert (test_pred.shape == test_indices.shape)
        predictions += [(index, pred) for index, pred in zip(test_indices, test_pred)]
        if type(_clf) == GridSearchCV:
            best_params_by_fold.append(_clf.best_params_)

        print("Finished fold #{0}".format(fold_num + 1))

    # sort predictions by their index in the original x
    predictions.sort(key=lambda tup: tup[0])
    assert(len(predictions) == len(y))

    # Print best parameters
    if len(best_params_by_fold) != 0:
        print("\tBest params by fold:")
    for fold_num, bp in enumerate(best_params_by_fold):
        print("\t\tFold #{0}:".format(fold_num + 1))
        for param, val in bp.items():
            print("\t\t\t{0}: {1}".format(param, val))

    # return just the ordered predictions, not the indices as well
    return [tup[1] for tup in predictions]


stops = set(stopwords.words("english"))
punct = set(string.punctuation)


def sent2vec(sent, w2v):
    words = [word for word in nltk.word_tokenize(sent) if word not in stops and word not in punct]
    res = np.zeros(w2v.vector_size)
    count = 0
    for word in words:
        if word in w2v:
            count += 1
            res += w2v[word]

    if count != 0:
        res /= count

    return res


def do_ml(X, y, base_fname):
    # print("\tLinear regression")
    # pipeline = Pipeline([('scale', preprocessing.StandardScaler()),
    #                    ('filter', SelectKBest(f_regression)),
    #                    ('lr', LinearRegression())])
    # pred = cross_val(pipeline, X, y)
    # print_error(pred, y)
    # write_predictions(pred, "LR", base_fname)
    #
    print("\tSVR grid:")
    pipeline = Pipeline([('scale', preprocessing.StandardScaler()),
                       ('filter', SelectKBest(f_regression)),
                       ('svr', SVR())])
    params = [{
        'svr__kernel': ['rbf'],
        'svr__C': np.logspace(-2, 2, 10),
        'svr__gamma': np.logspace(-3, 0.1, 10),
        "filter__k": ["all"]
    }]
    grid_search = GridSearchCV(pipeline, param_grid=params, scoring="mean_squared_error", n_jobs=8)
    pred = cross_val(grid_search, X, y)
    print_error(pred, y)
    write_predictions(pred, "SVR", base_fname)

    # print("\tRidge Regression")
    # pipeline = Pipeline([('scale', preprocessing.StandardScaler()),
    #                      ('filter', SelectKBest(f_regression)),
    #                      ('rr', Ridge())])
    # params = [{
    #     "rr__alpha": np.logspace(-2, 5, 50),
    #     "filter__k": ["all"],
    # }]
    # grid_search = GridSearchCV(pipeline, param_grid=params, scoring="mean_squared_error", n_jobs=8)
    # pred = cross_val(grid_search, X, y)
    # print_error(pred, y)
    # write_predictions(pred, "RR", base_fname)


def w2v_cosine(dataset, w2v):
    print("Doing ml experiments for w2v cosine")
    feats = list()
    for s1, s2 in zip(dataset["meta"]["sentence_1"], dataset["meta"]["sentence_2"]):
        v1 = sent2vec(s1, w2v)
        v2 = sent2vec(s2, w2v)
        sim = spatial.distance.cosine(v1, v2)
        feats.append([sim])

    return np.array(feats)


def w2v_concat(dataset, w2v):
    print("Doing ml experiments for concatenated w2v vectors")
    feats = list()
    for s1, s2 in zip(dataset["meta"]["sentence_1"], dataset["meta"]["sentence_2"]):
        v1 = sent2vec(s1, w2v)
        v2 = sent2vec(s2, w2v)
        feats.append(np.concatenate([v1, v2]))

    return np.array(feats)



    

def afs_with_w2v(dataset, w2v):
    print("Doing ml experiments for AFS features concatenated with w2v vectors")

    # Make vectorized AFS features
    vectorizer = DictVectorizer()
    dataset_features=build_allafsfeatures(dataset)
    afs_features = vectorizer.fit_transform(dataset_features["features"])

    # Make concatenated vectors
    feats = list()
    for afs_vec, s1, s2 in zip(afs_features, dataset["meta"]["sentence_1"], dataset["meta"]["sentence_2"]):
        v1 = sent2vec(s1, w2v)
        v2 = sent2vec(s2, w2v)
        # Get the dense (1, N) matrix, transpose it to (N, 1), and get the first row as a column vector
        afs_vec = afs_vec.todense().T.A1
        feats.append(np.concatenate([afs_vec, v1, v2]))

    return np.array(feats)


def afs_feature_by_key(dataset, feature_keys):
    print("Taking AFS features that start with: \"{0}\"".format(feature_keys))
    vectorizer = DictVectorizer()
    rouge_feats = [{key: val for key, val in feature_dict.items() if any(key.startswith(f_key) for f_key in feature_keys)}
                   for feature_dict in dataset["features"]]
    rouge_vecs = vectorizer.fit_transform(rouge_feats).todense()
    assert(len(rouge_feats) == len(dataset["features"]))
    return rouge_vecs


def write_feats(feats, fname):
    df = pandas.DataFrame(feats)
    with open(fname, 'w') as fh:
        df.to_csv(fh, index=False)


def write_predictions(predictions, ml_alg, base_fname):
    df = pandas.DataFrame(predictions, columns=["predicted_value"])
    full_fname = os.path.join(file_utilities.load_resource("scratch"), base_fname.format(ml_alg=ml_alg))
    with open(os.path.abspath(full_fname), 'w') as fh:
        df.to_csv(fh, index=False)


def run():
    # Load java AFS data
    data = load_afs_feature_data()
    # Load w2v model
    w2v = gensim.models.Word2Vec.load_word2vec_format(load_resource("w2v/w2v_300.model"))
    # w2v = gensim.models.Word2Vec.load_word2vec_format("/home/brian/nlp/tools/word2vec/trunk/GoogleNews-vectors-negative300.bin", binary=True)
    # print("Using word2vec model with {0} dimensional vectors".format(w2v.vector_size))

    for subset in ["train", "test"]:
        if subset != "train":
            continue
        for topic in ["GC", "GM", "DP"]:
            print("RESULTS FOR {0} {1} subset:".format(topic, subset))
            # features = afs_with_w2v(data[subset][topic], w2v)
            features = w2v_concat(data[subset][topic], w2v)
            # features = w2v_cosine(data[subset][topic], w2v)
            write_feats(features, os.path.join(file_utilities.load_resource("scratch"),
                                               "{0}_{1}_w2v{2}_afs_combined_features.csv".format(topic, subset, w2v.vector_size)))
            do_ml(features, data[subset][topic]["true_label"],
                  "{topic}_{subset}_{{ml_alg}}_w2v_afs_combined_predictions.csv".format(
                      topic=topic, subset=subset, ml_alg="ml_alg"))


if __name__ == "__main__":
    run()
