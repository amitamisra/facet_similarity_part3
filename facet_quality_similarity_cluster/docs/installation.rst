============
Installation
============

At the command line::

    $ easy_install afs_cluster

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv afs_cluster
    $ pip install afs_cluster
