"""
This module provides functions for:
 1.) Extracting features from text arguments.
 and/or
 2.) Using clustering algorithms to cluster arguments using passed features(May or may not come from the this module)
"""

import pandas as pd
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cluster import KMeans, MiniBatchKMeans, AffinityPropagation
from sklearn import metrics
import pickle


def clustering(X, num_clusters = 8, iter_max = 300, num_jobs = 1):
    km = KMeans(n_clusters= num_clusters, max_iter= iter_max, n_jobs= num_jobs)
    ap = AffinityPropagation()
    ap.fit(X)
    km.fit(X)
    print("Silhouette Coefficient Km: %0.3f"
      % metrics.silhouette_score(X, km.labels_))
    print("Silhouette Coefficient Ap: %0.3f"
      % metrics.silhouette_score(X, ap.labels_))
    # return km


def extract_features(text, stoplist = None, lower = True, max_feats = None, idf = True):
    count_vectorizer = CountVectorizer(stop_words= stoplist, lowercase= lower, max_features= max_feats).fit(text)
    X_count = count_vectorizer.transform(text)
    tfidf_transformer = TfidfTransformer().fit(X_count)
    X_tfidf = tfidf_transformer.transform(X_count)
    return X_count, X_tfidf


if __name__ == '__main__':
    sentences = pickle.load(open("test_sentences", "rb"))
    X_count, X_tfidf = extract_features(sentences)
    clustering(X_tfidf)