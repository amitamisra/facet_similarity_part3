'''
Created on Feb 8, 2016

@author: amita
'''

from sklearn.cluster import SpectralClustering
import utilities
from sklearn import metrics
class Spectralclass:
    def __init__(self,dataframe,similarity_col,id1,id2,n_clusters=8, affinity='precomputed',assign_labels='kmeans' ):
        self.dataframe= dataframe
        self.n_clusters=n_clusters
        self.affinity=affinity
        self.sim_col=similarity_col
        self.id1=id1
        self.id2=id2
        self.assign_labels=assign_labels
        
    def verifysymm_matrix(self): 
            print()
     
    
    def cluster(self):
        dictcluster=dict()
        df_symm=utilities.dataframe_to_symm_df(self.dataframe,self.sim_col,self.id1,self.id2)
        colnames_ids=df_symm.columns.values.tolist()
        symm_array_sim= df_symm.values
        labels= SpectralClustering(n_clusters=self.n_clusters, affinity=self.affinity).fit_predict(symm_array_sim)
        print (labels)
        print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(symm_array_sim, labels))
        # Number of clusters in labels, ignoring noise if present.
        records=len(df_symm)
        for count in range(0,records):
            dictcluster[colnames_ids[count]]=labels[count]
        print (dictcluster)    
        return   dictcluster      

        