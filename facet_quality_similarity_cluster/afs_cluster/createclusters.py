'''
Created on Jan 29, 2016
@author: amita
'''

import pandas as pd
import utilities

from DBSCANcluster import DBSCANclass
from Spectralcluster import Spectralclass


def runDBSCAN(input_csv_predict,input_csv_alldata,output_csv,sim_col,id1,id2):
    df_predict=pd.read_csv(input_csv_predict)
    df_sim = df_predict[[sim_col]]
    df_all=pd.read_csv(input_csv_alldata)
    frames=[df_sim,df_all]
    newdf=pd.concat(frames, axis=1)
    dbscanObj=DBSCANclass(newdf,sim_col,id1,id2,eps=4,min_samples=2,algorithm='auto', leaf_size=30) 
    dictcluster=dbscanObj.cluster()
    utilities.addclusterlabels(dbscanObj,dictcluster,id1,id2)
    dbscanObj.dataframe.to_csv(output_csv[:-4]+"_dbscan.csv",index =False) 

def runspectralCluster(input_csv_predict,input_csv_alldata,output_csv,sim_col,id1,id2):
    df_predict=pd.read_csv(input_csv_predict)
    df_sim = df_predict[[sim_col]]
    df_all=pd.read_csv(input_csv_alldata)
    frames=[df_sim,df_all]
    newdf=pd.concat(frames, axis=1)
    SpectralclassObj=Spectralclass(newdf,sim_col,id1,id2,n_clusters=8, affinity="precomputed")
    dictcluster=SpectralclassObj.cluster()
    utilities.addclusterlabels(SpectralclassObj,dictcluster,id1,id2)
    SpectralclassObj.dataframe.to_csv(output_csv[:-4]+"_spectral.csv",index =False) 

def readinput():
    input_csv_predict="/Users/amita/face_similarity_part3/guncontrol/gc_mini_test_features/Predict/gaussian/gc_mini_test_onlyfeatures_norm_47gaussianPukPredict.csv"
    input_csv_alldata="/Users/amita/face_similarity_part3/guncontrol/gc_mini_test_features/gc_mini_test.csv"
    output_csv="/Users/amita/face_similarity_part3/guncontrol/gc_mini_test_features/Predict/gaussian/gc_mini_test_onlyfeatures_norm_47gaussianPukPredict_cluster.csv"
    sim_col="regression_label"
    id1="uniqueRowNo_1"
    id2="uniqueRowNo_2"
    return (input_csv_predict,input_csv_alldata,output_csv,sim_col,id1,id2)
    
        
if __name__ == '__main__':
    (input_csv_predict,input_csv_alldata,output_csv,sim_col,id1,id2)= readinput()
    runDBSCAN(input_csv_predict,input_csv_alldata,output_csv,sim_col,id1,id2)
    runspectralCluster(input_csv_predict,input_csv_alldata,output_csv,sim_col,id1,id2)
    
    