import pandas as pd
import pickle
import numpy as np
from collections import Counter

# Initialization variables

n1 = 2  # Number of arguments to sample per post
n2 = 5  # Number of posts to sample per author per discussion
n3 = 5  # Number of arguments to sample per stance side from each discussion
n4 = 5  # Number of arguments to sample per discussion
aq = True  # Whether a min is being set for AQ or not
rand = True
aq_threshold = 0.55  # Threshold for AQ
# randomize the rows

# Either read the regular CSV with suffix "filtered_with_stance"
# df = pd.read_csv("../data/afs/gc_filtered_with_stance.csv", encoding='utf-8')
# Optionally randomize
# df_rand = df.reindex(np.random.permutation(df.index))
# df_rand.to_csv("../data/afs/gc_filtered_with_stance_rand.csv", encoding= "utf-8") # Save It

suffix = ""

# Otherwise just read the pre randomized one.
if rand:
    suffix += "_rand"
    df_rand = pd.read_csv("../resources/data/afs/gc_filtered_with_stance_rand.csv", encoding="utf-8")
else:
    df_rand = pd.read_csv("../resources/data/afs/gc_filtered_with_stance.csv", encoding='utf-8')
# ### Set threshold for AQ

if aq:
    suffix += "_" + str(aq_threshold)
    df_rand = df_rand[df_rand["Prediction"] >= aq_threshold]  # Threshold for AQ
# Optionally save it
# df_rand.to_csv("../data/afs/gc_filtered_with_stance_rand_55.csv", encoding="utf-8")


# ### Sampling by stance side (N3)

df_rand.drop('Unnamed: 0', axis=1, inplace=True)

df_stance_group = df_rand.dropna(subset=["topic_stance"]).sort(['datasetId', 'discussionId', 'topic_stance']).groupby(
    ['datasetId', 'discussionId', 'topic_stance'], as_index=False, sort=True)

df_stance_sampled = df_stance_group.head(n3)
print(df_stance_sampled.size)

df_stance_sampled.to_csv("../resources/data/sampled_files/gc_stance_sampled" + suffix + ".csv", encoding="utf_8")

# ### Sampling by post (N1)


df_post_group = df_rand.sort(['datasetId', 'discussionId', 'postId']).groupby(['datasetId', 'discussionId', 'postId'],
                                                                              as_index=False)
df_post_sampled = df_post_group.head(n1)
print(df_post_sampled.size)

df_post_sampled.to_csv("../resources/data/sampled_files/gc_post_sampled" + suffix + ".csv", encoding="utf-8")

# ### Sampling by author in a discussion (N2)


df_author_group = df_rand.sort(['datasetId', 'discussionId', 'authorId']).groupby(
    ['datasetId', 'discussionId', 'authorId'], as_index=False)
df_author_sampled = df_author_group.head(n2)
print (df_author_sampled.size)
df_author_sampled.to_csv("../resources/data/sampled_files/gc_author_sampled" + suffix + ".csv", encoding="utf-8")

# ### Sampling from discussion(N4)


df_discussion_group = df_rand.sort(['datasetId', 'discussionId']).groupby(['datasetId', 'discussionId'], as_index=False)
df_discussion_sampled = df_discussion_group.head(5)
print (df_discussion_sampled.size)
df_discussion_sampled.to_csv("../resources/data/sampled_files/gc_discussion_sampled" + suffix + ".csv", encoding="utf-8")
