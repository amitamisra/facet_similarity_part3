'''
Created on Feb 8, 2016

@author: amita
'''
from sklearn.cluster import DBSCAN
import utilities
from sklearn import metrics
class DBSCANclass:
    def __init__(self,dataframe,similarity_col,id1,id2,eps=0.5,min_samples=5,algorithm='auto', leaf_size=30 ):
        self.eps=eps
        self.min_samples=min_samples
        self.dataframe= dataframe
        self.algorithm=algorithm
        self.leaf_size=leaf_size
        self.sim_col=similarity_col
        self.id1=id1
        self.id2=id2
        
    def verifysymm_matrix(self): 
            print()
     
    def  changesimtodistance(self,symm_array_sim) :  
        symm_array_dist=5-symm_array_sim
        return symm_array_dist
    
    def cluster(self):
        dictcluster=dict()
        df_symm=utilities.dataframe_to_symm_df(self.dataframe,self.sim_col,self.id1,self.id2)
        colnames_ids=df_symm.columns.values.tolist()
        symm_array_sim= df_symm.values
        symm_array_dist=self.changesimtodistance(symm_array_sim)
        print (df_symm)
        print (symm_array_dist)
        labels= DBSCAN(eps=self.eps, min_samples=self.min_samples).fit_predict(symm_array_dist)
        print (labels)
        print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(symm_array_dist, labels))
        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        records=len(df_symm)
        for count in range(0,records):
            dictcluster[colnames_ids[count]]=labels[count]
        print (dictcluster)    
        return   dictcluster      
