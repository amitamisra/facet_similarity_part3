'''
Created on May 20, 2016

@author: amita
'''
import sys
import pandas as pd
from qual_sim_cluster_sharedutilities.db.db_connect import setup_connection
from qual_sim_cluster_sharedutilities import db_utilities
#input csv contains a key field, we parse that field to obtain dataset, discussion and postids for a dialog
#structure of key datasetid_discussionid_postid1,postid2,.....dialognumber
#there were several dialogs in a discussion
def createdialogfiles(sql_session,inputcsv,outputcsv,dataset):
    allrows=[]
    df=pd.read_csv(inputcsv)
    keys=df["key"].values
    for key in keys:
        rowdict=dict()
        keyseq=key.split("_")
        (dataset,discussion)=keyseq[0].split("-")
        datasetid=dataset
        discussionid=int(discussion)
        if int(dataset)!= 1:
            print("dataset not matching, exiting")
            sys.exit(-1)
        else:
            rowdict["datasetid"]=datasetid
            rowdict["discussonid"]= discussionid 
            postidlist=sorted([postid for postid in keyseq[1:-1] if postid])
            rowdict["postidlist"]=postidlist
            (S1,S2,posts_text)=addpostdetailsfromdb(sql_session,datasetid,discussionid,postidlist)
            rowdict["posttextlist"]=posts_text
            rowdict["S1"]=S1
            rowdict["S2"]=S2
            allrows.append(rowdict)
            
    dfwrite=pd.DataFrame(allrows)
    dfwrite.to_csv(outputcsv)
        


    
# return a string of post text changed to a dialog
def addpostdetailsfromdb(sql_session,dataset_id,discussion_id,post_idseq):
    alltext=""
    count=1
    author=1
    for post_id in post_idseq:
        text=db_utilities._get_post_text(sql_session, dataset_id, discussion_id, post_id)
        author_id=db_utilities._get_author_post(sql_session, dataset_id, discussion_id, post_id)
        text="<br><b>S{0}:{1}-  </b>{2}".format(author,count,text)
        alltext=alltext+ text
        if author==2:
            S2=author_id
            author=1
            count=count+1
        else:
            if author==1:    
                S1=author_id
                author=2
                
        
    return(S1,S2,alltext)    
       
     
if __name__ == '__main__':
    inputcsv="/Users/amita/git/FacetIdentification/data/dialog_data/CSV/abortion/MTdata/MTAll/Dialog_File_more750.csv"
    outputcsv="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/data/dialogdata/abortion/Dialog_File_more750.csv"
    _, sql_session = setup_connection()
    createdialogfiles(sql_session,inputcsv,outputcsv,1)