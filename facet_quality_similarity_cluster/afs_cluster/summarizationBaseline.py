'''
Created on May 19, 2016

@author: amita
'''
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import re
from gensim.summarization import summarize
from gensim.summarization import keywords
from bs4 import BeautifulSoup
import pandas as pd
ratio=0.1
def baseline(inputcsvdialogrows,outfile,rowcount):
    df=pd.read_csv(inputcsvdialogrows)
    rows=df.shape[0]
    dialogs=df["Dialogtext"].iloc[0:rowcount].values
    count=0
    textnoauthor=""
    dialogtext=""
    for dialog in dialogs:
        count=count+1
        dialog=dialog.replace(" .",".")
        dialog=dialog.replace(" ,",",")
        dialog=dialog.replace(" ?","?")
        dialog=dialog=dialog.replace(" / ","/")
        soup = BeautifulSoup(dialog, 'html.parser')
        dialog="<b>Dialog</b> {0}".format(count)+dialog +"<br>"
        dialogtext=dialogtext+dialog
        textnoauthor=textnoauthor+re.sub("S[0-9]:[0-9]-","",soup.text)
    Html_file= open(outfile,"w")
    Html_file.write(dialogtext)
    Html_file.write("<br>")
    summary=str(summarize(textnoauthor, ratio=ratio))
    keywordlist=str(keywords(textnoauthor, ratio=ratio))
    pattern="S[0-9]"
    Html_file.write("<br><b>Summarized dialog with summarization ratio as {0}</b><br>".format(ratio))
    patternmatches=[(m.start(0), m.end(0)) for m in re.finditer(pattern, summary)]
    Html_file.write(summary)
    Html_file.write("<br><br> <b>keywords:</b> <br>")
    Html_file.write(keywordlist)
    Html_file.close()
    print (patternmatches)

if __name__ == '__main__':
    inputcsvdialogrows="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/data/dialogdata/sampledialogs.csv"
    outfile="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/data/dialogdata/dialogsummarybaseline.html"
    rowcount=10
    baseline(inputcsvdialogrows,outfile,rowcount)