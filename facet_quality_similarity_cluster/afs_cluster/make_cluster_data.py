"""
Provides a way to create csvs with combinations of unseen instances.
"""

from itertools import combinations

import numpy as np
import pandas

from file_utilities import get_used_data_lookup, load_high_quality_instances, get_output_file


def get_unseen_data(df, data_col, num_instances):
    """
    Filters an input DataFrame to remove all previously used instances of data, samples a certain number of instances,
    makes combinations from those instances, and then returns a DataFrame containing the combinations.

    :param df: The input file to take instances from
    :param data_col: The name of the data column containing the input sentence
    :param num_instances: The number of instances to make combinations from. Use None if you wish to use all instances.
    :return: A DataFrame with (num_instances Choose 2) rows. Each row is a combination of two rows.
    """

    # Excluded data lookup from past experiments
    excluded_data_lookup = get_used_data_lookup()

    # Create a new header for combinations
    header = list(df.columns.values)
    new_header = ["{0}_1".format(col) for col in header] + ["{0}_2".format(col) for col in header]

    # Exclude some based on them being previously used in other experiments
    filtered_df = df[df[data_col].map(lambda x: True if x not in excluded_data_lookup else False)]

    # Sample num_instances from the filtered_df with fixed random_state
    sampled_df = filtered_df.sample(n=num_instances, random_state=100)

    # Make pairs
    combs = combinations(sampled_df.iterrows(), 2)

    # Unpack tuple combinations
    rows = [np.concatenate([entry[0][1].values, entry[1][1].values]) for entry in combs]

    return pandas.DataFrame(rows, columns=new_header)

if __name__ == "__main__":
    # Write small test set
    data = get_unseen_data(load_high_quality_instances()["GC"], "sentence", num_instances=40)
    data.to_csv(get_output_file("gc_mini_test.csv"), index_label="unique_combination_id")
