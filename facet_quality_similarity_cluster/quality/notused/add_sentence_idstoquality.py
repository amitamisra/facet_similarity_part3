'''
Created on May 25, 2016
add sentence ids to training data
@author: amita
'''
import pandas as pd,sys
def addsentenceids(allsentences_csv,inputcsv,outputcsv):
    sentence_idlist=[]
    df_all=pd.read_csv(allsentences_csv)
    df=pd.read_csv(inputcsv)
    for index, row in df.iterrows():
        sentence=df.at[index,"Phrase.x"]
        datasetId=df.at[index,"Dataset_Id"]
        discussionId=df.at[index,"Discussion_Id"]
        postId=df.at[index,"Post_Id"]
        matchrow=df_all[(df_all['sentence'] == sentence) & (df_all['datasetId']==datasetId) & (df_all['discussionId']==discussionId) & (df_all['postId']==postId)]
        
        if len(matchrow ) > 1:
            print(sentence)
            print("error")
            sys.exit(-1)
            
        if len(matchrow ) == 0 :
            print("sentence not in database {0}".format(sentence))   
        else:
            sentence_id= matchrow.iloc[0]["sentenceId"]   
            sentence_idlist.append(sentence_id)
    df["sentenceId"]=sentence_idlist
    df.rename(columns={'Dataset_Id': 'datasetId', 'Discussion_Id': 'discussionId','Post_Id':'postId'}, inplace=True)

    df.to_csv(outputcsv)
if __name__ == '__main__':
    allsentences_csv="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/quality/data/deathpenalty/dp_all_sentences_with_predictions.csv"
    inputcsv="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/old_training_data/facet_quality/dp-slider-means.training.pmi.csv"
    outputcsv="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/old_training_data/facet_quality/dp-slider-means.training.pmi_id.csv"
        
    addsentenceids(allsentences_csv,inputcsv,outputcsv)