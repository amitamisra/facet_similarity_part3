#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_afs_cluster
----------------------------------

Tests for `facet_quality_similarity_cluster` module.
"""

import unittest

from afs_cluster import afs_cluster


class TestAfs_cluster(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_000_something(self):
        pass


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
