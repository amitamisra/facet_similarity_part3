===============================
afscluster
===============================

.. image:: https://img.shields.io/travis/amitamisra/afs_cluster.svg
        :target: https://travis-ci.org/amitamisra/afs_cluster

.. image:: https://img.shields.io/pypi/v/afs_cluster.svg
        :target: https://pypi.python.org/pypi/afs_cluster


clustering similar facets using AFS

* Free software: ISC license
* Documentation: https://afs_cluster.readthedocs.org.

Features
--------
http://mccormickml.com/2017/01/11/word2vec-tutorial-part-2-negative-sampling/
neg=5 based on this.
1) Run extract_sentences to get all sentences from database.
2) Run filter train sentences /Users/amita/git/argument_summarization_17/src/filter_previous_training_data.py 
to filter posts used in signal 2015.
3)Run doc2vec_extractor.py to create different doc2_vec models by changing dm ,hs.
4)Run ridge regression with CV to determine best parameters for doc2vec model
5) Use this doc2vec model for further processing
6)use the best model from here to see how you perform on test set by combining
word2vec+best model
7)
