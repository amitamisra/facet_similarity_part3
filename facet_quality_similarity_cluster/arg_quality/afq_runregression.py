'''
Created on Jun 1, 2016

@author: amita
'''
from os import path
import configparser,os
from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities import stanfordparser_utility
import pandas as pd
import feature_util_afs
import afqregression
from sklearn import  preprocessing
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.pipeline import Pipeline
from sklearn.svm import SVR
import afq_features
from sklearn.feature_extraction import DictVectorizer
import logging
import codecs
from pprint import pprint
import numpy as np

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

pipeline = Pipeline([('scale', preprocessing.StandardScaler()),
                       ('filter', SelectKBest(f_regression)),
                       ('svr', SVR())])
num_features="all"
gammastart=-3
gammaend=0.1
gammacount=10
Cstart=-2
Cend=2
Ccount=10

alphastart=-2
alphaend=5
alphacount=50

def doridge_reg(X_train,Y_train,X_test,Y_test,unsupervised_allfeatures, unsupervised_labels,param_dict_regression,train_df,test_df,unsupervised_df):
    ridgeregressor= afqregression.RidgeRegression(num_features,alphastart,alphaend, alphacount)
    ridgeregressor.setpipeline()
    ridgeregressor.setparam_grid()
    classifier="ridge"
    result_dict_cv={}
    result_dict_test={}
    feature="_".join(param_dict_regression["feature_list"])
    if param_dict_regression["eval_type"]=="CV":
            best_param,Y_pred=afqregression.nestedgridSearchcvpredict(ridgeregressor.pipeline,ridgeregressor.param_grid,"mean_squared_error",X_train,Y_train)
            result_dict_cv=afqregression.calculatemetrics(Y_train,Y_pred)
            result_dict_cv["best_param"]=best_param
            base_dir=param_dict_regression["outputBaseDir"]
            pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_train, Y_pred)]
            csvFile=os.path.join(base_dir,feature+classifier+"__CV.csv")
            writeresultstoFile(csvFile,result_dict_cv, pred_list,X_train,Y_train,param_dict_regression)
            new_df=train_df.assign(Y_predicted=Y_pred)
            with_pred_file=os.path.join(base_dir,feature+classifier+"__CVpred.csv")
            new_df.to_csv(with_pred_file,index=False)
            
    if param_dict_regression["eval_type"]=="Test":
            best_param,Y_pred=afqregression.GridSearchCVTest(ridgeregressor.pipeline,ridgeregressor.param_grid,"mean_squared_error",X_train,Y_train,X_test,Y_test)
            result_dict_test=afqregression.calculatemetrics(Y_test,Y_pred)
            result_dict_test["best_param"]=best_param
            base_dir=param_dict_regression["outputBaseDir"]
            pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_test, Y_pred)]
            csvFile=os.path.join(base_dir,feature+classifier+"__test.csv")
            writeresultstoFile(csvFile,result_dict_test, pred_list,X_test,Y_test,param_dict_regression)
            new_df=test_df.assign(Y_predicted =Y_pred)
            with_pred_file=os.path.join(base_dir,feature+classifier+"__testpred.csv")
            new_df.to_csv(with_pred_file,index=False)
            
    if param_dict_regression["eval_type"]=="Predict":
            X_unsuper=unsupervised_allfeatures
            Y_unsuper=unsupervised_labels
            best_param,Y_pred=afqregression.GridSearchCVTest(ridgeregressor.pipeline,ridgeregressor.param_grid,"mean_squared_error",X_train,Y_train,X_unsuper,Y_unsuper)
            result_dict_test=afqregression.calculatemetrics(Y_unsuper,Y_pred)
            result_dict_test["best_param"]=best_param
            base_dir=param_dict_regression["outputBaseDir"]
            pred_list= [{'Y_unsuper': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_unsuper, Y_pred)]
            csvFile=os.path.join(base_dir,feature+classifier+"__onlypredict.csv")
            writeresultstoFile(csvFile,result_dict_test, pred_list,X_unsuper,Y_unsuper,param_dict_regression)
            new_df=unsupervised_df.assign(Y_predicted =Y_pred)
            predictedfile=param_dict_regression["unsupervised_base"]
            with_pred_file=os.path.join(base_dir,predictedfile+"_"+feature+classifier+"__with_pred.csv")
            new_df.to_csv(with_pred_file,index=False)         
    
    return(result_dict_cv,result_dict_test)  
  
def dosvm_reg(X_train,Y_train,X_test,Y_test,unsupervised_allfeatures, unsupervised_labels,param_dict_regression,train_df,test_df,unsupervised_df):
    svmregressor= afqregression.SVMRegression(num_features,gammastart,gammaend,gammacount,Cstart,Cend,Ccount)
    svmregressor.setpipeline()
    svmregressor.setparam_grid()
    result_dict_cv={}
    result_dict_test={}
    result_dict_predict={}
    classifier="svm"
    feature="_".join(param_dict_regression["feature_list"])
    if param_dict_regression["eval_type"]=="CV":
            logger.info("doing CV")
            best_param,Y_pred=afqregression.nestedgridSearchcvpredict(svmregressor.pipeline,svmregressor.param_grid,"mean_squared_error",X_train,Y_train)
            result_dict_cv=afqregression.calculatemetrics(Y_train,Y_pred)
            result_dict_cv["best_param"]=best_param
            base_dir=param_dict_regression["outputBaseDir"]
            pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_train, Y_pred)]
            csvFile=os.path.join(base_dir,feature+classifier+"__CV.csv")
            writeresultstoFile(csvFile,result_dict_cv, pred_list,X_train,Y_train,param_dict_regression)
            new_train_df=train_df.assign(Y_predicted=Y_pred)
            with_pred_file=os.path.join(base_dir,feature+classifier+"__CVpred.csv")
            new_train_df.to_csv(with_pred_file,index=False)
            
            
    if param_dict_regression["eval_type"]=="Test":
            logger.info("starting test set")
            best_param,Y_pred=afqregression.GridSearchCVTest(svmregressor.pipeline,svmregressor.param_grid,"mean_squared_error",X_train,Y_train,X_test,Y_test)
            result_dict_test=afqregression.calculatemetrics(Y_test,Y_pred)
            result_dict_test["best_param"]=best_param
            base_dir=param_dict_regression["outputBaseDir"]
            pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_test, Y_pred)]
            csvFile=os.path.join(base_dir,feature+classifier+"__test.csv")
            writeresultstoFile(csvFile,result_dict_test, pred_list,X_test,Y_test,param_dict_regression)
            new_test_df=test_df.assign(Y_predicted =Y_pred)
            with_pred_file=os.path.join(base_dir,feature+classifier+"__testpred.csv")
            new_test_df.to_csv(with_pred_file,index=False)
    
    
    if param_dict_regression["eval_type"]=="Predict":
        # for predict, we do not know true values in Y_test.
        logger.info("starting only predictions set")
        X_unsuper=unsupervised_allfeatures
        Y_unsuper=unsupervised_labels
        best_param,Y_pred=afqregression.GridSearchCVTest(svmregressor.pipeline,svmregressor.param_grid,"mean_squared_error",X_train,Y_train,X_unsuper,Y_unsuper)
        #result_dict_test=afqregression.calculatemetrics(Y_test,Y_pred)
        result_dict_predict["best_param"]=best_param
        base_dir=param_dict_regression["outputBaseDir"]
        pred_list= [{'Y_unsuper': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_unsuper, Y_pred)]
        csvFile=os.path.join(base_dir,feature+classifier+"_predict.csv")
        writeresultstoFile(csvFile,result_dict_predict, pred_list,X_unsuper,Y_unsuper,param_dict_regression)
        new_un_df=unsupervised_df.assign(Y_predicted =Y_pred)
        predictedfile=param_dict_regression["unsupervised_base"]
        with_pred_file=os.path.join(base_dir,predictedfile+"_"+feature+classifier+"__with_pred.csv")
        new_un_df.to_csv(with_pred_file,index=False)
        
    return(result_dict_cv,result_dict_test) 


def writeresultstoFile(csvFile,result_dict,pred_list,X_train,Y_train,param_dict_regression):
    logger.info("start writing file " + str(param_dict_regression["feature_list"]) +" "+csvFile  )
    assert(len(pred_list)==len(X_train)==len(Y_train))
    df1=pd.DataFrame(pred_list)
    df2=pd.DataFrame(X_train)
    df = df2.join(df1)
    df.to_csv(csvFile,index=False)
    f = codecs.open(csvFile, "a",encoding='utf-8')
    pprint(result_dict, stream=f) 
    pprint(param_dict_regression["feature_list"], stream=f)
    f.close()
    logger.info("Done writing file " + str(param_dict_regression["feature_list"]) +" "+csvFile  )
    
    

def doml(X_train,Y_train,X_test,Y_test, unsupervised_allfeatures, unsupervised_labels,param_dict_regression,train_df,test_df,unsupervised_df):
    if param_dict_regression["reg_type"]=="ridge" :
        result_dict_cv, result_dict_test=doridge_reg(X_train,Y_train,X_test,Y_test,unsupervised_allfeatures, unsupervised_labels,param_dict_regression,train_df,test_df,unsupervised_df)
        return(result_dict_cv, result_dict_test)
    if param_dict_regression["reg_type"]=="svm" :
        result_dict_cv, result_dict_test=dosvm_reg(X_train,Y_train,X_test,Y_test,unsupervised_allfeatures, unsupervised_labels,param_dict_regression,train_df,test_df,unsupervised_df)
        return(result_dict_cv, result_dict_test)
    
    
def run():
    all_list=[]
    config = configparser.ConfigParser()
    current_dir=path.abspath(path.dirname(__file__))
    config.read(path.abspath(path.join(current_dir,"afq_runregression_config.ini")))
    df=file_utilities.load_aq_data()
    parser=""
    #stanfordparser_utility.getparse()
    for topic in ["GC"]:
        regressioncsv=config.get(topic,"afq_runregressioncsv")
        resultcsv=config.get(topic,"result_csv")
        inputdata_df=pd.read_csv(regressioncsv,na_filter=False)
        all_list=[]
        if os.path.exists(resultcsv):
                os.remove(resultcsv)
        for index, row in inputdata_df.iterrows():
            param_dict_regression={}
            done=row["done"]
            if int(done)==1:
                continue
            param_dict_regression={}
            train_df=df["train"][topic]
            test_df=df["test"][topic]
            param_dict_regression["topic"]=topic
            param_dict_regression["reg_type"]=row["reg_type"]
            param_dict_regression["feature_list"]=list(row["feature_list"].split(","))
            param_dict_regression["eval_type"]=row["eval_type"]
            param_dict_regression["regression_label"]=row["regression_label"]
            param_dict_regression["Num_features_selection"]=list(row["Featureselection"].split(","))# A list of possible  feature count  for feature selection, by default no feature selection
            param_dict_regression["doc2vecmodel"]=row["doc2vecmodel"]
            param_dict_regression["outputBaseDir"]=row["outputBaseDir"]
            param_dict_regression["w2vec_loc"]=row["w2vec_loc"]
            train_labels=train_df["GoodSliderMean"].values
            test_labels=test_df["GoodSliderMean"].values
            if str(row["unsupervised"])==str(1):
                unsupervised_file=row["unsupervised_loc"]
                unsupervised_df=pd.read_csv(unsupervised_file)
                unsupervised_df.sentence = unsupervised_df.sentence.fillna('')
            else:
                unsupervised_df=""    
            
            unsupervised_loc=os.path.basename(row["unsupervised_loc"])    
            param_dict_regression["unsupervised_base"]=unsupervised_loc
            train_alldicts,test_alldicts,unsupervised_alldicts=afq_features.createfeatures(train_df,test_df,unsupervised_df,parser,param_dict_regression)
            v = DictVectorizer(sparse=False)
            train_allfeatures=v.fit_transform(train_alldicts)
            test_allfeatures=v.transform(test_alldicts)
            if str(row["unsupervised"])==str(1):
                unsupervised_allfeatures=v.transform(unsupervised_alldicts)
                unsupervised_labels=np.zeros(len(unsupervised_df))
            else:
                    unsupervised_allfeatures=[]
                    unsupervised_labels=np.zeros()
            
            
            
            #print(unsupervised_allfeatures[1:10][1:10])
            result_dict_CV,result_dict_Test=doml(train_allfeatures,train_labels,test_allfeatures,test_labels, unsupervised_allfeatures, unsupervised_labels,param_dict_regression,train_df,test_df,unsupervised_df)
            addresult(result_dict_CV,result_dict_Test,param_dict_regression,all_list)
        if all_list:
            df_all=pd.DataFrame(all_list)
            df_all.to_csv(resultcsv,index=False)   
            
             
             
def createresultdict(param_dict_regression,result_dict,eval_type):   
    newdict={}
    newdict["topic"]=param_dict_regression["topic"] 
    newdict["reg_type"]=param_dict_regression["reg_type"]
   # newdict["feature_cols_included"]=param_dict_regression["feature_cols_included"]
    newdict["feature_list"]=str(param_dict_regression["feature_list"])
    newdict["rmse"]=result_dict["rmse"]
    newdict["rrse"]=result_dict["rrse"]
    newdict["rsquare"]=result_dict["rsquare"]
    newdict["r"]=result_dict["r"]
    newdict["Eval_type"]=eval_type
    #newdict["param_grid"]=result_dict["param_grid"]
    newdict["best_param"]=result_dict["best_param"]
    return newdict

def addresult(result_dict_CV,result_dict_Test,param_dict_regression,all_list):
  
    if result_dict_CV:
        newdict=createresultdict(param_dict_regression,result_dict_CV,"CV")
        all_list.append(newdict)
    if result_dict_Test:
        newdicttest=createresultdict(param_dict_regression,result_dict_Test,"test")
        all_list.append(newdicttest)       
if __name__ == '__main__':
    run()