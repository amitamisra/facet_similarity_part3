'''
Created on May 13, 2016

@author: amita
'''
def build_ngrams(annotation, tokens_key, order):
    """
    Build all ngrams from an annotation dictionary
    :param annotation: The annotation dictionary from stanford parser
    :param tokens_key: The dictionary key in each sentence dictionary that gives tokens, e.g. "lemmas" or "tokens"
    :param order: The n in ngram
    :return: A list of all ngrams made within each sentence in all sentences
    """
    ngrams = list()
    for sentence in annotation["sentences"]:
        ngrams += ngram_combinations(sentence[tokens_key], order)

    return ngrams

def ngram_combinations(tokens, order):
    """
    Make ngram style combinations from the tokens
    :param tokens: A list of units to make into ngrams
    :param order: The n in ngram
    :return: A list of tuples
    """
    results = list()
    for i in range(0, len(tokens)):
        n = order if i + order < len(tokens) else len(tokens) - i
        for j in range(1, n + 1):
            results.append(tuple(tokens[k] for k in range(i, i + j)))

    return results