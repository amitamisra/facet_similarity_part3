'''
Created on May 4, 2016

@author: amita
'''

import string
from nltk.corpus import stopwords
from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities.stanford_corenlp_pywrapper.sockwrap import CoreNLP
from qual_sim_cluster_sharedutilities import stanfordparser_utility
import logging
from nltk import word_tokenize
from doc2vec_extractor import CustomDoc2vec
from  word2vec_extractor import Word2vecExtractor
import pandas as pd
import gensim


stops = set(stopwords.words("english"))
punct = set(string.punctuation)
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


global w2vobject
global customdoc2vec
def loadresourcesrequired(param_dict_regression):
    
    global w2vobject
    global customdoc2vec
    if "Googleword2vec" in param_dict_regression["feature_list"]:
        w2vec_location=param_dict_regression["w2vec_loc"]
        #gensim.models.Word2Vec.load_word2vec_format(w2vecmodel,binary=True)
        #w2v = gensim.models.Word2Vec.load_word2vec_format(w2vec_location.strip(),binary=True)
        w2vobject= Word2vecExtractor(w2vec_location)
    if "Doc2vec" in param_dict_regression["feature_list"]:
        customdoc2vec=CustomDoc2vec()
        customdoc2vec.load(param_dict_regression["doc2vecmodel"])
            

    
def createfeatures(train_df,test_df,unsupervised_df,parser,param_dict_regression):
    """ 
    label should be consistent wrt to label used while training the doc2vec model.
    Here it assumes that label is train_0, train1 etc... for training data, 
    test_0,test_1....  for testdata 
    unsupervised_0...  for unlabelled data. 
    """
    loadresourcesrequired(param_dict_regression)
    train_texts=train_df["Phrase.x"].values
    test_texts=test_df["Phrase.x"].values
    if isinstance(unsupervised_df, pd.DataFrame):
        unsupervised_texts=unsupervised_df["sentence"].values
        unsupervised=True
    else:
            unsupervised=False
    
    train_alldicts=[]
    test_alldicts=[]
    unsupervised_alldicts=[]
    featurelist=param_dict_regression["feature_list"]
    
    count=0
    for traintext in train_texts:
        feature_dict_train={}
        label="train_"+str(count)
        count=count+1
        if "Googleword2vec" in featurelist:
            feature_dictw2vec=w2vobject.extract(traintext)      
            feature_dict_train.update(feature_dictw2vec)
             
        if "Doc2vec" in featurelist:  
            train_doc2vec_features=customdoc2vec.docfeatures(label)
            feature_dict_doc2vec=customdoc2vec.features_todict(train_doc2vec_features)
            feature_dict_train.update(feature_dict_doc2vec)
        
        train_alldicts.append(feature_dict_train)
        
    count=0    
    for test_text in test_texts:
        feature_dict_test={}
        label="test_"+str(count)
        count=count+1
        if "Googleword2vec" in featurelist:
            feature_dicttestw2vec=w2vobject.extract(test_text)
            feature_dict_test.update(feature_dicttestw2vec)
             
            
        if "Doc2vec" in featurelist:     
            test_doc2vec_features=customdoc2vec.docfeatures(label)
            feature_dict_doc2vec=customdoc2vec.features_todict(test_doc2vec_features)
            feature_dict_test.update(feature_dict_doc2vec)
        test_alldicts.append(feature_dict_test) 
     
    count=0  
    if unsupervised:  
        for unsupervised_text in unsupervised_texts:
            feature_dict_unsupervised={}
            label="unsupervised_"+str(count)
            count=count+1
            if "Googleword2vec" in featurelist:
                print(unsupervised_text)
                feature_dict_unsupervised_w2vec=w2vobject.extract(unsupervised_text) 
                feature_dict_unsupervised.update(feature_dict_unsupervised_w2vec)
                 
                
            if "Doc2vec" in featurelist:     
                unsupervised_doc2vec_features=customdoc2vec.docfeatures(label)
                feature_dict_doc2vec=customdoc2vec.features_todict(unsupervised_doc2vec_features)
                feature_dict_unsupervised.update(feature_dict_doc2vec)
            unsupervised_alldicts.append(feature_dict_unsupervised)       
        
    return(train_alldicts, test_alldicts,unsupervised_alldicts)    
    
    
def run():
    pass
if __name__ == '__main__':
    run()
    