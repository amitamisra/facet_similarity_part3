'''
Created on May 31, 2016

@author: amita
'''
'''
Created on May 30, 2016

@author: amita
'''
from qual_sim_cluster_sharedutilities import file_utilities
import afqregression
import afq_runregression
import os 
from sklearn.feature_extraction import DictVectorizer
import pandas as pd
import configparser
from os import path
from doc2vec_extractor import CustomDoc2vec
import numpy as np
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
num_features="all"
gammastart=-3
gammaend=0.1
gammacount=10
Cstart=-2
Cend=2
Ccount=10
alphastart=-2
alphaend=5
alphacount=50

def mlCV(X_train,Y_train,X_test,Y_test,param_dict_regression):
    ridgeregressor= afqregression.RidgeRegression(num_features,alphastart,alphaend, alphacount)
    ridgeregressor.setpipeline()
    ridgeregressor.setparam_grid()
    param_dict_regression["reg_type"]="ridge"
    best_param,Y_pred=afqregression.nestedgridSearchcvpredict(ridgeregressor.pipeline,ridgeregressor.param_grid,"mean_squared_error",X_train,Y_train)
    result_dict=afqregression.calculatemetrics(Y_train,Y_pred)
    result_dict["best_param"]=best_param
    base_dir=param_dict_regression["outputBaseDir"]
    pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_train, Y_pred)]
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)
        
    csvFile=os.path.join(base_dir, param_dict_regression["modelname"][:-4]+".csv")
    afq_runregression.writeresultstoFile(csvFile,result_dict,pred_list,X_train,Y_train,param_dict_regression)
    return result_dict

def testdoc2vec():
    
    config = configparser.ConfigParser()
    current_dir=path.abspath(path.dirname(__file__))
    config.read(path.abspath(path.join(current_dir,"testdoc2vec_config.ini")))
    df=file_utilities.load_aq_data()
    for topic in ["EVO"]:
        param_dict_regression={}
        allmodel_results=[]
        resultsfile=config.get(topic,"resultsfile")
        param_dict_regression["outputBaseDir"]=config.get(topic,"outputBaseDir")
        dirname=config.get(topic,"modeldir")
        train_df=df["train"][topic]
        test_df=df["test"][topic]
        param_dict_regression["topic"]=topic
        for filename  in os.listdir(dirname):
            if str(filename).endswith(".model"):
                #print(topic)
                #print("features for model {0}".format(filename))
                filenamewithpath=os.path.join(dirname,filename)
                customdoc2vec=CustomDoc2vec()
                customdoc2vec.load(filenamewithpath)
                train_texts=train_df["Phrase.x"].values
                train_labels=train_df["GoodSliderMean"].values
                test_texts=test_df["Phrase.x"].values
                test_labels=test_df["GoodSliderMean"].values
                train_allfeatures=[]
                test_allfeatures=[]
                count=0
                for traintext in train_texts:
                    label="train_"+str(count)
                    train_doc2vec_features=customdoc2vec.docfeatures(label)
                    feature_dicttrain=customdoc2vec.features_todict(train_doc2vec_features)
                    train_allfeatures.append(feature_dicttrain)
                    count=count+1
                    
                count=0    
                for text in test_texts:
                    label="test_"+str(count)   
                    test_doc2vec_features=customdoc2vec.docfeatures(label)
                    feature_dicttest=customdoc2vec.features_todict(test_doc2vec_features)
                    test_allfeatures.append(feature_dicttest)    
                    count=count+1
                
            #param_dict_regression["featureList"]=["Googleword2vec"]  
                param_dict_regression["feature_list"]=[filename +"Doc2vec"]  
                v = DictVectorizer(sparse=False)
                train_allfeatures_arr=v.fit_transform(train_allfeatures)
                test_allfeatures_arr=v.transform(test_allfeatures)
                param_dict_regression["modelname"]=filename
                result_dict=mlCV(train_allfeatures_arr,train_labels,test_allfeatures_arr,test_labels,param_dict_regression)  
                result_dict["modelname"]=filename
                allmodel_results.append(result_dict)
        allresults_sorted=sorted(allmodel_results,key=lambda x: float(x['rmse'])) 
        resdf=pd.DataFrame(allresults_sorted)
        resdf.to_csv(resultsfile)
if __name__ == '__main__':
    testdoc2vec()
