'''
Created on May 4, 2016

@author: amita
'''

import string
from nltk.corpus import stopwords
from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities.stanford_corenlp_pywrapper.sockwrap import CoreNLP
from qual_sim_cluster_sharedutilities import stanfordparser_utility
import logging
from nltk import word_tokenize
from doc2vec_extractor import CustomDoc2vec
import pandas as pd
import gensim
import configparser
from os import path
from word2vec_extractor import Word2vecExtractor
from timeit import default_timer as timer
import file_utilities


stops = set(stopwords.words("english"))
punct = set(string.punctuation)
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

features=["w2vec","skip-thought","doc2vec"]
         
def addw2vecfeatures(sentence,W2vecextractor):
    w2vec_scores=W2vecextractor.extract(sentence)
    return w2vec_scores
    

def applyfeatures(x,datacolumn,W2vecextractor):    
    if "w2vec" in features:
        w2vec_scores=addw2vecfeatures(x[datacolumn],W2vecextractor)
        return w2vec_scores
        
def buildfeatures(traindf,testdf,features_train,features_test,datacolumn,w2vec_loc,srparser_loc,parse_model):
    if "w2vec" in features:
        W2vecextractor=Word2vecExtractor(w2vec_loc)   
    else:
        W2vecextractor=""                                       
    
    if  "skip-thought" in features:
        #addskipfeatures(input_file,datacolumn1)
        pass
        
    start = timer()
    train_features=traindf.apply(applyfeatures,args=(datacolumn,W2vecextractor),axis=1)
    test_features=testdf.apply(applyfeatures,args=(datacolumn,W2vecextractor),axis=1)
    train_features.fillna(0,inplace=True)
    merged_df_train= traindf.join(train_features)
    merged_df_test= testdf.join(test_features)
    end = timer()
    print("feature generation for {0}took".format(str(features)))
    print(end - start)      
    #merged_df.to_csv(features_file)

    
def run(topic_list):
    config = configparser.ConfigParser()
    current_dir=path.abspath(path.dirname(__file__))
    for topic in topic_list:
        config.read(path.abspath(path.join(current_dir,"afq_featues_config.ini")))
        train=config.get(topic,"train")
        test=config.get(topic,"test")
        features_train=config.get(topic,"features_train")
        features_test=config.get(topic,"features_test")
        datacolumn=config.get(topic," datacolumn")
        w2vec_loc=config.get(topic,"w2vec_loc")
        srparser_loc=config.get(topic," srparser_loc")
        parse_model=config.get(topic,"parse_model")
        traindf=pd.read_csv(train)
        testdf=pd.read_csv(test)
        buildfeatures(traindf,testdf,features_train,features_test,datacolumn,w2vec_loc,srparser_loc,parse_model)
        
if __name__ == '__main__':
    run()
    