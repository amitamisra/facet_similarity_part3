'''
Created on Jun 1, 2016

@author: amita
'''
from os import path
import sys,os
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
arg_quality_DIR=path.abspath((path.dirname(__file__)))
import pandas as pd
from facet_quality_similarity_cluster.qual_sim_cluster_sharedutilities.db.iac_objects import _discussion
from facet_quality_similarity_cluster.qual_sim_cluster_sharedutilities.db.db_connect import setup_connection

def getsentences(sql_session,topic):
    all_sentences=[]
    results = list(sql_session.execute("""
        select topic_id
        from topics
        where
            topic = :topic 
    """, {"topic": topic}))
    
    if len(results) == 1:
        topic_id= results[0][0]
    elif len(results) > 1:
        raise "Query returned too many results"
        sys.exit(-1)
        
    discussions = (sql_session.query(_discussion.Discussion)
    
                   .filter(_discussion.Discussion.topic_id == topic_id)
                   .all())
    count=0
    for discussion in discussions:
        count=count+1
        no_disc=len(discussions)
        for post in discussion.get_posts():
            discussion_id=post.discussion_id
            post_id=post.post_id
            dataset_id=post.dataset_id
            # load parse data if possible
            if post.text_obj.load_parse_data():
                sentences= post.text_obj.sentences
                for sentence in sentences:
                    newddict={"Discussion_Id":discussion_id,"Post_Id":post_id}
                    sentence.update(newddict)
                    sentence["sentence"] = sentence.pop("text")
                    sentence["Dataset_Id"] = sentence.pop("dataset_id")
                    sentence["sentence_Id"] = sentence.pop("sentence_index")
                    
                    sentence.pop('tokens', None)
                    all_sentences.append(sentence)
        logger.info("done adding sentences for {0} of {1} discussions,Most recent added: dataset_id {2},discussion id {3}".format(count,no_disc,dataset_id,discussion_id))          
    return all_sentences

if __name__ == '__main__':
    
    # change topic in these 4 lines, topic corresponds to topic in database"
    #topic_list=["gay marriage", "gun control", "death penalty"]
    topic_list=["evolution"]
    for topic in topic_list:
        outputdir_name="".join(topic.split())
        outputfile_name=outputdir_name +"_db_allsentences.csv"
        output_dir = path.join(arg_quality_DIR,"data",outputdir_name)
        outfile=path.join(output_dir,outputfile_name)
        
        
        if not path.exists(output_dir):
            os.makedirs(output_dir)
        _, sql_session = setup_connection()
        all_sentences=getsentences(sql_session,topic)
        df= pd.DataFrame(all_sentences)
        df.to_csv(outfile,index=False)