'''
Created on May 12, 2016

@author: amita
'''

from sklearn import preprocessing, clone, svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
import numpy as np, os
import pandas as pd
from sklearn.cross_validation import KFold
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from numpy.ma import corrcoef
from math import sqrt
import logging
import codecs
from sklearn import cross_validation

from pprint import pprint
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
class SVMRegression:
    
    def __init__(self,numFeatureSelection,gammastart,gammaend,gammacount,Cstart,Cend,Ccount):
        self.gammastart=float(gammastart)
        self.gammaend=float(gammaend)
        self.gammacount=float(gammacount)
        self.Cstart=float(Cstart)
        self.Cend=float(Cend)
        self.Ccount=float(Ccount)
        self.numFeatureSelection= [int(x) for x in numFeatureSelection if x.isdigit()] + ["all"]
    def setpipeline(self):
        self.pipeline=Pipeline([('scale', preprocessing.StandardScaler()),('filter', SelectKBest(f_regression)),('svr', svm.SVR())])
        
    def setparam_grid(self):
        C_range = np.logspace(self.Cstart, self.Cend, self.Ccount)
        gamma_range = np.logspace(self.gammastart,self.gammaend,self.gammacount)
        self.param_grid=[{'svr__kernel': ['rbf'], 'svr__gamma': gamma_range,'svr__C': C_range, 'filter__k': self.numFeatureSelection}]
        
        
class RidgeRegression:
    def __init__(self,numFeatureSelection,alphastart,alphaend, alphacount):
        self.numFeatureSelection= [int(x) for x in numFeatureSelection if x.isdigit()] + ["all"]
        self.alphastart=float(alphastart)
        self.alphaend=float(alphaend)
        self.alphacount=alphacount
      
    def setpipeline(self):
        self.pipeline=Pipeline([('scale', preprocessing.StandardScaler()),('filter', SelectKBest(f_regression)),('rr',Ridge())])
        
    def setparam_grid(self):
        alpha_range = np.logspace(self.alphastart, self.alphaend, self.alphacount)
        self.param_grid=[{'filter__k': self.numFeatureSelection, "rr__alpha":alpha_range}] 


def calculatemetrics(Y_test, Y_pred):
    result_dict={}
    result_dict["r"] =corrcoef(Y_test,Y_pred)[0, 1]
    rmse=result_dict["rmse"]=sqrt(mean_squared_error(Y_test,Y_pred))
    result_dict["rsquare"]=r2_score(Y_test,Y_pred)
    rrse = rmse / np.sqrt(mean_squared_error(Y_test, np.repeat(Y_test.mean(), len(Y_test))))
    result_dict["rrse"]=rrse
    return result_dict


def nestedgridSearchcvpredict(pipelineclf,param_grid,score,X,Y):
    grid_search = GridSearchCV(pipelineclf, param_grid=param_grid,scoring=score,cv=5)
    best_params={}
    Y_pred=cross_validation.cross_val_predict(grid_search, X, Y,cv=10)
    return(best_params,Y_pred)

def nestedGridSearchCV(pipelineclf,param_grid,score,X,Y):
    """
    Do nested cross validation over 10 folds
    :param clf:
    :param x:
    :param y:
    :return: Return predictions and a best parameters dictionary. If clf is not a GridSearchCV, then the best parameters
    dict will be empty.
    It does a 5 fold cv to determine best parameters within a fold
    """
    gridclf = GridSearchCV(pipelineclf,param_grid=param_grid,scoring=score,cv=5)
    skfold = KFold(len(X), n_folds=10, shuffle=True, random_state=123)
    predictions = list()
    best_params_by_fold = list()
    for fold_num, indices in enumerate(skfold):
        train_indices = indices[0]
        test_indices = indices[1]
        _clf = clone(gridclf)
        _clf.fit(X[train_indices], Y[train_indices])
        test_pred = _clf.predict(X[test_indices])

        assert (test_pred.shape == test_indices.shape)
        predictions += [(index, pred) for index, pred in zip(test_indices, test_pred)]
        if type(_clf) == GridSearchCV:
            best_params_by_fold.append(_clf.best_params_)

        #print("Finished fold #{0}".format(fold_num + 1))

    # sort predictions by their index in the original x
    predictions.sort(key=lambda tup: tup[0])
    assert(len(predictions) == len(Y))
    
    best_params=[]
    # Print best parameters
    if len(best_params_by_fold) != 0:
        best_params.append("\tBest params by fold:")
    for fold_num, bp in enumerate(best_params_by_fold):
        best_params.append("\t\tFold #{0}:".format(fold_num + 1))
        for param, val in bp.items():
            best_params.append("\t\t\t{0}: {1}".format(param, val))

    # return just the ordered predictions, not the indices as well
    return (best_params,[tup[1] for tup in predictions])

# Use it to do a grid search to get best parameters and then use them to evaluate a set set
def GridSearchCVTest(pipeline, param_grid, score, X_train,Y_train,X_test,Y_test):
    grid_search = GridSearchCV(pipeline, param_grid=param_grid,scoring=score,cv=5)       
    grid_search.fit(X_train,Y_train)
    best_param=grid_search.best_params_
    Y_pred=grid_search.predict(X_test)
    return (best_param,Y_pred)


if __name__ == '__main__':
    pass