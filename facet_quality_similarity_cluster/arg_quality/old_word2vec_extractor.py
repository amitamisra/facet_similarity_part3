'''
Created on May 31, 2016
create a word2vec model. 
@author: amita
'''
import numpy as np 

class CustomWord2vec:
    def __init__(self,w2vec):
        self.w2v=w2vec 
    def buildword2vec(self,words):
        """
        use the w2vec model to build feature vector, average the word vectors
        """
        res = np.zeros(self.w2v.vector_size)
        count = 0
        for word in words:
            if word in self.w2v:
                count += 1
                res += self.w2v[word]
        if count != 0:
            res /= count
        return res
    def features_todict(self,w2vec_features):    
        number_w2vec=w2vec_features.size     
        columnnames=["w2vec_"+ str(i) for i in range(0, number_w2vec)]  
        feature_dict=dict(zip(columnnames,w2vec_features)) 
        return feature_dict
if __name__ == '__main__':
    pass