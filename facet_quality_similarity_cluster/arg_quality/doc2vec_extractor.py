'''
Created on May 24, 2016

@author: amita
'''
import logging
from qual_sim_cluster_sharedutilities import file_utilities
import codecs
import feature_util_afs
import afqregression
import gensim.models.doc2vec
import pandas as pd 
import re
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from random import shuffle
import json
import os
import configparser
from os import path

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.WARNING)
logging.root.handlers[0].level = logging.WARNING


lowercase=True
removepunctuation=True
# these were set based on CV results of ridge regression for gc,dp,gm. Do not change
hs=0
neg=5
dm=0
minc=2
PROJECT_DIR = (path.abspath(path.join(path.dirname(__file__), "..")))

class CustomDoc2vec:
    
    """ 
    build custom doc2vec, by default removepunctuation,lowercase are set to True 
    to split a sentence into tokens use gensim 
    """
    def __init__(self,removepunctuation=True,lowercase=True):
        """
        bool:removepunctuation
        bool:lowercase
        """
        self.removepunctuation=removepunctuation
        self.lowercase=lowercase
    
    def doc2vec_preprocess(self,sentence):
        if self.removepunctuation:
            sentence = re.sub("[^a-zA-Z]"," ", sentence)
        if   self.lowercase:
            sentence=str(sentence).lower()   
        return sentence
 
 

    def addtagdocdata(self,df,split,alldocs):
        """gensim expects a tag for each document,
            df: dataframe
            alldocs: tagged documents append to alldocs
            split: train/test/unupervised
        These tags are important as we do randomization while training the model
        hence these tags are assigned sequentially  in the beginning before randomization.
        Use them to get the features for each document
        """

        count=0
        if split=="train" or split=="test":
            sentences=df["Phrase.x"].values
        else:
            sentences=df["sentence"].values
        for sentence in sentences:    
                #print(sentence)
                sentence=self.doc2vec_preprocess(sentence)    
                tokens = gensim.utils.to_unicode(sentence).split()
                tag=split+"_"+str(count)
                count=count+1
                alldocs.append(TaggedDocument(tokens, [tag]))


    def createtaggedDoc(self,unsupervised_df,train_df,test_df):
        """
        create tagged documents for train, test and unsupervised
        for train, it is train_0, train_1  ....
        for test,  it is test_0, test_1 .....
        for unlabelled it is unsupervised_0, unsupervised_1
        """
        alldocs=[]
        if isinstance(unsupervised_df, pd.DataFrame):
            self.addtagdocdata(unsupervised_df,"unsupervised",alldocs)
        self.addtagdocdata(train_df,"train",alldocs)
        self.addtagdocdata(test_df,"test",alldocs)
        return alldocs

    def load(self,filenamewithpath):
        self.model=Doc2Vec.load(filenamewithpath)
    
    def docfeatures(self,label):
        feature_vector=self.model.docvecs[label]
        return feature_vector
    def features_todict(self,doc2vec_features):    
        number_doc2vec=doc2vec_features.size     
        columnnames=["doc2vec_"+ str(i) for i in range(0,number_doc2vec)]  
        feature_dict=dict(zip(columnnames,doc2vec_features)) 
        return feature_dict

def addsampletomodel(modeldir,modelname,model,unsupervised_df):
    strlist=[]   
    sample="as far as jesus was conerned any act of any sin brought the death penalty upon you"
    strlist.append("similar vectors to inferred vector for\n {0} ".format(sample))
    words=sample.split()
    filename=os.path.join(modeldir, modelname+"doc2vec.txt") 
    inferred_docvec = model.infer_vector(words,steps=10,alpha=.0001)
    inf_vector_sim= model.docvecs.most_similar([inferred_docvec], topn=3)
    strlist.extend( inf_vector_sim)
    strlist.append("unsupervised_0\n")
    strlist.append(unsupervised_df.iloc[0]["sentence"])
    unsup_0=model.docvecs.most_similar("unsupervised_0",topn=3)
    strlist.append("similar vectors to unsupervised_0")
    strlist.extend(unsup_0)
    with codecs.open(filename,"w",encoding="utf-8") as f:
        json.dump(strlist, f)
        f.write("\n")

def createdoc2vec(unsupervised_df,train_df,test_df,topic,modeldir):
    """
    Each model is saved in modeldir
    fixed parameters for AQ, default
    hs=0
    neg=5
    dm=0
    minc=2
    saves a model with name as dm"+str(dm)+"win"+ str(windowsize)+"dim"+str(dimsize)+"hs"+str(hs)+"min_count" + str(minc)
    """
    assert gensim.models.doc2vec.FAST_VERSION > -1
    custom_model=CustomDoc2vec(removepunctuation,lowercase)
    alldocs=custom_model.createtaggedDoc(unsupervised_df,train_df,test_df)
    
    for windowsize in range(2,3):
        logger.info("start building the doc2vecmodel for windowsize: " + str(windowsize))
        for dimsize in range(200,201,100):
                            model=Doc2Vec(dm=dm, size=dimsize, window=windowsize, negative=neg, hs=hs, min_count=minc,workers=4)
                            modelname="dm"+str(dm)+"win"+ str(windowsize)+"dim"+str(dimsize)+"hs"+str(hs)+"min_count" + str(minc)
                            model.build_vocab(alldocs)  
                            for epoch in range(20):
                                shuffle(alldocs)
                                model.train(alldocs)
                                model.alpha -= 0.001 # decrease the learning rate
                                model.min_alpha = model.alpha # fix the learning rate, no decay
                                model.train(alldocs)
                            
                            
                            modelfile=os.path.join(modeldir, modelname+"doc2vec.model")    
                            model.save(modelfile)
                            #addsampletomodel(modeldir,modelname,model,unsupervised_df)
        logger.info("finished building the doc2vecmodel for windowsize: " + str(windowsize))
        
def run():
    config = configparser.ConfigParser()
    current_dir=path.abspath(path.dirname(__file__))
    config.read(path.abspath(path.join(current_dir,"doc2vec_extractor_config.ini")))
    df=file_utilities.load_aq_data()
    for topic in ["GM"]:
        unsupervisedfilename=config.get(topic,"unsupervised")
        if unsupervisedfilename:
            unsupervised_df=pd.read_csv(unsupervisedfilename)
            unsupervised_df.sentence = unsupervised_df.sentence.fillna('')
        else:
                unsupervised_df=""
        param_dict_regression={}
        traindf=df["train"][topic]
        testdf=df["test"][topic]
        param_dict_regression["topic"]=topic
        modeldir=config.get(topic,"modeldir")
        createdoc2vec(unsupervised_df,traindf,testdf,topic,modeldir)
        
    
if __name__ == '__main__':
    run()
    
        