"""
Add dataset, discussion, post id to the slider means argument quality training and testing files in the resources
directory.

Expects the files in drive at:
    IAC Discourse Connectives > data > facet existence > AMT Batch Files > aq_batch_files.tar.xz
to be placed in resources/scratch/aq_batch_files/
"""
import os

import itertools
import re

import pandas as pd

from qual_sim_cluster_sharedutilities import file_utilities


def batch_translation():
    """
    Build a translation dictionary of phrase -> id tuple using the batch files
    :return: A translation dict
    """
    batch_dir = file_utilities.load_resource("scratch", "aq_batch_files")
    phrase_to_id = dict()
    for f in os.listdir(batch_dir):
        print(f)
        with open(os.path.join(batch_dir, f), 'r', encoding="utf-8") as fh:
            df = pd.read_csv(fh, encoding="utf-8")

        id_vars = list()
        for i in range(1, 21):
            for col in df.columns:
                if any(x in col for x in ["Dataset_Id", "Discussion_Id", "Post_Id"]):
                    id_vars.append(col)
                if "Disucssion_Id" in col:
                    id_vars.append(col.replace("Disuccsion", "Discussion"))
                if "phrase" in col:
                    id_vars.append(col)

        # Group id_vars into keys
        id_vars = [tuple(g) for k, g in itertools.groupby(id_vars, key=lambda x: re.search("[0-9]+", x).group(0))]
        for da, di, po, ph in id_vars:
            for da_v, di_v, po_v, ph_v in zip(df[da], df[di], df[po], df[ph]):
                phrase_to_id[ph_v.strip()] = (da_v, di_v, po_v)

    return phrase_to_id


def run():
    phrase_to_id = batch_translation()
    aq_dir = file_utilities.load_resource("old_training_data", "facet_quality")
    for f in os.listdir(aq_dir):
        print(f)
        with open(os.path.join(aq_dir, f), 'r', encoding="utf-8") as fh:
            df = pd.read_csv(fh, encoding="utf-8")

        da = list()
        di = list()
        po = list()
        for phrase in df["Phrase.x"]:
            ids = phrase_to_id[phrase.strip()]
            da.append(ids[0])
            di.append(ids[1])
            po.append(ids[2])

        df["Dataset_Id"] = da
        df["Discussion_Id"] = di
        df["Post_Id"] = po

        df.to_csv(os.path.join(aq_dir, f), index=False)
        df.to_csv("/home/brian/test.csv", index=False)


if __name__ == "__main__":
    run()
